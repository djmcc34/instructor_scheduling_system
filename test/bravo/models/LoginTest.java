/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Spaceman
 */
public class LoginTest {

    Login instance = null;
    Integer LOGINID = 1;
    String EMAILADDRESS = "test@test.com";
    String PASSWORD = "password";

    public LoginTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        instance = new Login();
        instance.setEmailAddress(EMAILADDRESS);
        instance.setPassword(PASSWORD);
        instance.setLoginId(LOGINID);
    }

    @After
    public void tearDown() {
    }
    
    /**
     * Test empty constructor, of class Login
     */
    @Test
    public void testConstructor() {
        Login newLogin = new Login();
        assertNotNull(newLogin);
        assertNull(newLogin.getEmailAddress());
        assertNull(newLogin.getPassword());
        assertNull(newLogin.getLoginId());
    }

    /**
     * Test of getLoginId method, of class Login.
     */
    @Test
    public void testGetLoginId() {
        Integer result = instance.getLoginId();
        assertEquals(result, LOGINID);
    }

    /**
     * Test of setLoginId method, of class Login.
     */
    @Test
    public void testSetLoginId() {
        Integer expectedResult = 77;
        instance.setLoginId(expectedResult);
        Integer result = instance.getLoginId();
        assertEquals(result, expectedResult);
    }

    /**
     * Test of getEmailAddress method, of class Login.
     */
    @Test
    public void testGetEmailAddress() {
        String result = instance.getEmailAddress();
        assertEquals(result, EMAILADDRESS);
    }

    /**
     * Test of setEmailAddress method, of class Login.
     */
    @Test
    public void testSetEmailAddress() {
        String expectedResult = "new@new.com";
        instance.setEmailAddress(expectedResult);
        String result = instance.getEmailAddress();
        assertEquals(result, expectedResult);
    }

    /**
     * Test of getPassword method, of class Login.
     */
    @Test
    public void testGetPassword() {
        String result = instance.getPassword();
        assertEquals(result, PASSWORD);
    }

    /**
     * Test of setPassword method, of class Login.
     */
    @Test
    public void testSetPassword() {
        String expectedResult = "a new password";
        instance.setPassword(expectedResult);
        String result = instance.getPassword();
        assertEquals(result, expectedResult);
    }
}
