/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author masud
 */
public class DepartmentTest {
    
    Department instance = null;
    String SHORTNAME = "TEST";
    Integer DEPARTMENTID = 1;
    String DEPARTMENTNAME = "test dept";
    boolean ISDELETED = false;
    
    public DepartmentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Department();
        instance.setDepartmentId(DEPARTMENTID);
        instance.setDepartmentName(DEPARTMENTNAME);
        instance.setIsDeleted(ISDELETED);
        instance.setShortName(SHORTNAME);
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test empty constructor, of class Department.
     */
    @Test
    public void testConstructor() {
        Department newDepartment = new Department();
        String departmentName = newDepartment.getDepartmentName();
        Integer departmentId = newDepartment.getDepartmentId();
        String shortName = newDepartment.getShortName();
        assertNotNull(newDepartment);
        assertNull(departmentName);
        assertNull(departmentId);
        assertNull(shortName);
        assertFalse(newDepartment.getIsDeleted());
    }
    
    /**
     * Test constructor with department id, of class Department.
     */
    @Test
    public void testConstructorWithDepartmentId() {
        Department newDepartment = new Department(DEPARTMENTID);
        assertNotNull(newDepartment);
        Integer id = newDepartment.getDepartmentId();
        assertEquals(id, DEPARTMENTID);
    }
    
    /**
     * Test constructor with department id and department name and isDeleted, of class Department.
     */
    @Test
    public void testConstructorWithDepartmentIdAndDepartmentNameAndIsDeleted() {
        Department newDepartment = new Department(DEPARTMENTID, DEPARTMENTNAME, ISDELETED);
        assertNotNull(newDepartment);
        Integer id = newDepartment.getDepartmentId();
        String deptName = newDepartment.getDepartmentName();
        boolean isDeleted = newDepartment.getIsDeleted();
        assertEquals(id, DEPARTMENTID);
        assertEquals(deptName, DEPARTMENTNAME);
        assertFalse(isDeleted);
    }

    /**
     * Test of getDepartmentId method, of class Department.
     */
    @Test
    public void testGetDepartmentId() {
        Integer result = instance.getDepartmentId();
        assertEquals(DEPARTMENTID, result);
    }

    /**
     * Test of setDepartmentId method, of class Department.
     */
    @Test
    public void testSetDepartmentId() {
        Integer expectedResult = 999;
        instance.setDepartmentId(expectedResult);
        Integer result = instance.getDepartmentId();
        assertEquals(result, expectedResult);
    }

    /**
     * Test of getDepartmentName method, of class Department.
     */
    @Test
    public void testGetDepartmentName() {
        String result = instance.getDepartmentName();
        assertEquals(DEPARTMENTNAME, result);
    }

    /**
     * Test of setDepartmentName method, of class Department.
     */
    @Test
    public void testSetDepartmentName() {
        String expectedResult = "a new test dept name";
        instance.setDepartmentName(expectedResult);
        String result = instance.getDepartmentName();
        assertEquals(result, expectedResult);
    }

    /**
     * Test of getIsDeleted method, of class Department.
     */
    @Test
    public void testGetIsDeleted() {
        assertFalse(instance.getIsDeleted());
    }

    /**
     * Test of setIsDeleted method, of class Department.
     */
    @Test
    public void testSetIsDeleted() {
        boolean expectedResult = true;
        instance.setIsDeleted(expectedResult);
        boolean result = instance.getIsDeleted();
        assertEquals(result, expectedResult);
    }

    /**
     * Test of getShortName method, of class Department.
     */
    @Test
    public void testGetShortName() {
        String result = instance.getShortName();
        assertEquals(SHORTNAME, result);
    }

    /**
     * Test of setShortName method, of class Department.
     */
    @Test
    public void testSetShortName() {
        String expectedResult = "TEST";
        instance.setShortName(expectedResult);
        String result = instance.getShortName();
        assertEquals(result, expectedResult);
    }
}
