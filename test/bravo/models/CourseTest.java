/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Spaceman
 */
public class CourseTest {

    Course course = null;
    String DESCRIPTION = "A Test Course";
    String SHORTTITLE = "Intro to a Test Course";
    Integer ID = 1;
    Integer CREDITHOURS = 4;
    Integer COURSENUMBER = 111;
    Integer DEPARTMENTID = 6;
    String DEPARTMENTNAME = "Computer Science";
    String DEPARTMENTSHORTNAME = "COMP";
    boolean ISDELETED = false;

    public CourseTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        course = new Course(ID, SHORTTITLE);
        course.setDescription(DESCRIPTION);
        course.setCreditHours(CREDITHOURS);
        course.setCourseNumber(COURSENUMBER);
        course.setDepartmentId(DEPARTMENTID);
        course.setDepartmentName(DEPARTMENTNAME);
        course.setDepartmentShortName(DEPARTMENTSHORTNAME);
        course.setIsDeleted(ISDELETED);
    }

    @After
    public void tearDown() {
        course = null;
    }

    
    /**
     * Test empty constructor for class Course.
     */
    @Test
    public void testConstructor(){
        Course newCourse = new Course();
        assertNotNull(newCourse);
        assertNull(newCourse.getDescription());
        assertNull(newCourse.getShortTitle());
        assertNull(newCourse.getId());
        assertNull(newCourse.getCreditHours());
        assertNull(newCourse.getCourseNumber());
        assertNull(newCourse.getDepartmentId());
        assertNull(newCourse.getDepartmentName());
        assertNull(newCourse.getDepartmentShortName());
        assertFalse(newCourse.isDeleted());
    }
    
    
    /**
     * Test constructor with id for class Course.
     */
    @Test
    public void testConstructorWithId(){
        Course newCourse = new Course(ID);
        assertNotNull(newCourse);
        Integer id = newCourse.getId();
        assertEquals(ID, id);
    }
    
    /**
     * Test constructor with id and short title for class Course.
     */
    @Test
    public void testConstructorWithIdAndShortTitle(){
        Course newCourse = new Course(ID, SHORTTITLE);
        assertNotNull(newCourse);
        Integer id = newCourse.getId();
        String shortTitle = newCourse.getShortTitle();
        assertEquals(ID, id);
        assertEquals(SHORTTITLE, shortTitle);
    }
    
    /**
     * Test of getDescription method, of class Course.
     */
    @Test
    public void testGetDescription() {
        String result = course.getDescription();
        assertEquals(DESCRIPTION, result);
    }

    /**
     * Test of setDescription method, of class Course.
     */
    @Test
    public void testSetDescription() {
        String description = "A new description";
        course.setDescription(description);
        String result = course.getDescription();
        assertEquals(description, result);
    }

    /**
     * Test of isDeleted method, of class Course.
     */
    @Test
    public void testIsDeleted() {
        assertFalse(course.isDeleted());
    }

    /**
     * Test of setIsDeleted method, of class Course.
     */
    @Test
    public void testSetIsDeleted() {
        course.setIsDeleted(true);
        assertTrue(course.isDeleted());
    }

    /**
     * Test of getShortTitle method, of class Course.
     */
    @Test
    public void testGetShortTitle() {
        String result = course.getShortTitle();
        assertEquals(SHORTTITLE, result);
    }

    /**
     * Test of setShortTitle method, of class Course.
     */
    @Test
    public void testSetShortTitle() {
        String shortTitle = "A new short title";
        course.setShortTitle(shortTitle);
        String result = course.getShortTitle();
        assertEquals(shortTitle, result);
    }

    /**
     * Test of getId method, of class Course.
     */
    @Test
    public void testGetId() {
        Integer id = course.getId();
        assertEquals(id, ID);
    }

    /**
     * Test of setId method, of class Course.
     */
    @Test
    public void testSetId() {
        Integer id = 10;
        course.setId(id);
        Integer result = course.getId();
        assertEquals(id, result);
    }

    /**
     * Test of getCreditHours method, of class Course.
     */
    @Test
    public void testGetCreditHours() {
        Integer creditHours = course.getCreditHours();
        assertEquals(creditHours, CREDITHOURS);
    }

    /**
     * Test of setCreditHours method, of class Course.
     */
    @Test
    public void testSetCreditHours() {
        Integer creditHours = 6;
        course.setCreditHours(creditHours);
        Integer result = course.getCreditHours();
        assertEquals(creditHours, result);
    }

    /**
     * Test of getCourseNumber method, of class Course.
     */
    @Test
    public void testGetCourseNumber() {
        Integer courseNumber = course.getCourseNumber();
        assertEquals(courseNumber, COURSENUMBER);
    }

    /**
     * Test of setCourseNumber method, of class Course.
     */
    @Test
    public void testSetCourseNumber() {
        Integer courseNumber = 222;
        course.setCourseNumber(courseNumber);
        Integer result = course.getCourseNumber();
        assertEquals(courseNumber, result);
    }

    /**
     * Test of getDepartmentId method, of class Course.
     */
    @Test
    public void testGetDepartmentId() {
        Integer expectedResult = course.getDepartmentId();
        assertEquals(expectedResult, DEPARTMENTID);
    }

    /**
     * Test of setDepartmentId method, of class Course.
     */
    @Test
    public void testSetDepartmentId() {
        Integer expectedResult = 90;
        course.setDepartmentId(expectedResult);
        Integer result = course.getDepartmentId();
        assertEquals(expectedResult, result);
    }

    /**
     * Test of getDepartmentName method, of class Course.
     */
    @Test
    public void testGetDepartmentName() {
        String expectedResult = course.getDepartmentName();
        assertEquals(expectedResult, DEPARTMENTNAME);
    }

    /**
     * Test of setDepartmentName method, of class Course.
     */
    @Test
    public void testSetDepartmentName() {
        String expectedResult = "Web Development";
        course.setDepartmentName(expectedResult);
        String result = course.getDepartmentName();
        assertEquals(expectedResult, result);
    }
}
