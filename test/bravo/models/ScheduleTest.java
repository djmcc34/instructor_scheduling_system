/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joshua Lynn
 */
public class ScheduleTest {
    Schedule schedule = null;
    final Integer SCHEDULEID = 1;
    final String TERM = "Spring";
    final int YEAR = 2019;
    final String SECTION = "Q1WW";
    final Date STARTDATE = new Date();
    final Date ENDDATE = new Date();
    final Date STARTTIME = new Date();
    final Date ENDTIME = new Date();
    final String DAYS = "Monday, Tuesday";
    final String STATUS = "Pending";
    final boolean ISDELETED = false;
    final String LOCATION = "Online";
    
    
    public ScheduleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        schedule = new Schedule();
        Course course = new Course();
        schedule.setCourse(course);
        schedule.setCourseDays(DAYS);
        schedule.setEndDate(ENDDATE);
        schedule.setEndTime(ENDTIME);
        Instructor instructor = new Instructor();
        schedule.setInstructor(instructor);
        schedule.setIsDeleted(ISDELETED);
        schedule.setLocation(LOCATION);
        schedule.setScheduleId(SCHEDULEID);
        schedule.setSection(SECTION);
        schedule.setStartDate(STARTDATE);
        schedule.setStartTime(STARTTIME);
        schedule.setStatus(STATUS);
        schedule.setTerm(TERM);
        schedule.setYear(YEAR);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Instructor Constructors
     */
    @Test
    public void testScheduleConstructors() {
        System.out.println("Testing Schedule Constructors");
        System.out.println("Testing Default Constructor");
        assertNotNull(schedule);
        
        System.out.println("Testing Constructor with ID");
        Schedule s2 = new Schedule(1);
        assertNotNull(s2);
        Integer result = s2.getScheduleId();
        assertEquals("Incorrect Schedule Id", s2.getScheduleId(), result);
        
        System.out.println("Testing Constructor with ID, Term, Section, Location, StartDate, EndDate, Status, IsDeleted");
        Schedule s3 = new Schedule(SCHEDULEID, TERM, SECTION, LOCATION, STARTDATE, ENDDATE, STATUS, ISDELETED);
        assertNotNull(s3);
        assertEquals("Incorrect Term", s3.getTerm(), TERM);
        assertEquals("Incorrect ScheduleID", s3.getScheduleId(), SCHEDULEID);
        assertEquals("Incorrect Section", s3.getSection(), SECTION);
        assertEquals("Incorrect Location", s3.getLocation(), LOCATION);
        assertEquals("Incorrect Start Date", s3.getStartDate(), STARTDATE);
        assertEquals("Incorrect End Date", s3.getEndDate(), ENDDATE);
        assertEquals("Incorrect Status", s3.getStatus(), STATUS);
        assertFalse("Should not be Deleted", s3.getIsDeleted());
        
    }
    
    /**
     * Test of getScheduleId method, of class Schedule.
     */
    @Test
    public void testGetScheduleId() {
        System.out.println("getScheduleId");
        Integer expResult = SCHEDULEID;
        Integer result = schedule.getScheduleId();
        assertEquals(expResult, result);

    }

    /**
     * Test of setScheduleId method, of class Schedule.
     */
    @Test
    public void testSetScheduleId() {
        System.out.println("setScheduleId");
        Integer scheduleId = 500;
        schedule.setScheduleId(scheduleId);
        Integer result = schedule.getScheduleId();
        assertEquals("Incorrectly set Schedule ID", result, scheduleId);
    }

    /**
     * Test of getTerm method, of class Schedule.
     */
    @Test
    public void testGetTerm() {
        System.out.println("getTerm");
        String expResult = "Spring";
        String result = schedule.getTerm();
        assertEquals("Incorrect Term", expResult, result);

    }

    /**
     * Test of setTerm method, of class Schedule.
     */
    @Test
    public void testSetTerm() {
        System.out.println("setTerm");
        String term = "Winter";
        schedule.setTerm(term);
        assertEquals("Term Set Incorrectly", schedule.getTerm(), term);
    }

    /**
     * Test of getYear method, of class Schedule.
     */
    @Test
    public void testGetYear() {
        System.out.println("getYear");
        int expResult = 2019;
        int result = schedule.getYear();
        assertEquals("Year was incorrect", expResult, result);

    }

    /**
     * Test of setYear method, of class Schedule.
     */
    @Test
    public void testSetYear() {
        System.out.println("setYear");
        int year = 2020;
        schedule.setYear(year);
        assertEquals("Year set incorrectly", schedule.getYear(), year);
    }

    /**
     * Test of getSection method, of class Schedule.
     */
    @Test
    public void testGetSection() {
        System.out.println("getSection");
        String expResult = "Q1WW";
        String result = schedule.getSection();
        assertEquals("The section was incorrect", expResult, result);

    }

    /**
     * Test of setSection method, of class Schedule.
     */
    @Test
    public void testSetSection() {
        System.out.println("setSection");
        String section = "JW10";
        schedule.setSection(section);
        assertEquals("Incorrectly set section", schedule.getSection(), section);
    }

    /**
     * Test of getLocation method, of class Schedule.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        String expResult = "Online";
        String result = schedule.getLocation();
        assertEquals("Incorrect Location", expResult, result);

    }

    /**
     * Test of setLocation method, of class Schedule.
     */
    @Test
    public void testSetLocation() {
        System.out.println("setLocation");
        String location = "Downtown";
        schedule.setLocation(location);
        assertEquals("Location set incorrectly", schedule.getLocation(), location);
    }

    /**
     * Test of getStartDate method, of class Schedule.
     */
    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");

        Date expResult = STARTDATE;
        Date result = schedule.getStartDate();
        assertEquals("The Start Date was Incorrect", expResult, result);

    }

    /**
     * Test of setStartDate method, of class Schedule.
     */
    @Test
    public void testSetStartDate() {
        System.out.println("setStartDate");
        Date startDate = new Date();
        schedule.setStartDate(startDate);
        assertEquals("The Start Date was incorrectly set", schedule.getStartDate(), startDate);
    }

    /**
     * Test of getEndDate method, of class Schedule.
     */
    @Test
    public void testGetEndDate() {
        System.out.println("getEndDate");
        Date expResult = ENDDATE;
        Date result = schedule.getEndDate();
        assertEquals("The End date is incorrect", expResult, result);

    }

    /**
     * Test of setEndDate method, of class Schedule.
     */
    @Test
    public void testSetEndDate() {
        System.out.println("setEndDate");
        Date endDate = new Date();
        schedule.setEndDate(endDate);
        assertEquals("The End date was set incorrectly", schedule.getEndDate(), endDate);
    }

    /**
     * Test of getStartTime method, of class Schedule.
     */
    @Test
    public void testGetStartTime() {
        System.out.println("getStartTime");
        Date expResult = STARTTIME;
        Date result = schedule.getStartTime();
        assertEquals("The start time is incorrect", expResult, result);

    }

    /**
     * Test of setStartTime method, of class Schedule.
     */
    @Test
    public void testSetStartTime() {
        System.out.println("setStartTime");
        Date startTime = new Date();
        schedule.setStartTime(startTime);
        assertEquals("Incorrect start time set", schedule.getStartTime(), startTime);
    }

    /**
     * Test of getEndTime method, of class Schedule.
     */
    @Test
    public void testGetEndTime() {
        System.out.println("getEndTime");
        Date expResult = ENDTIME;
        Date result = schedule.getEndTime();
        assertEquals("End Time was incorrect", expResult, result);

    }

    /**
     * Test of setEndTime method, of class Schedule.
     */
    @Test
    public void testSetEndTime() {
        System.out.println("setEndTime");
        Date endTime = new Date();
        schedule.setEndTime(endTime);
        assertEquals("Incorrect End time was set", schedule.getEndTime(), endTime);
    }

    /**
     * Test of getCourseDays method, of class Schedule.
     */
    @Test
    public void testGetCourseDays() {
        System.out.println("getCourseDays");
        String expResult = DAYS;
        String result = schedule.getCourseDays();
        assertEquals("The course days were Wrong", expResult, result);
    }

    /**
     * Test of setCourseDays method, of class Schedule.
     */
    @Test
    public void testSetCourseDays() {
        System.out.println("setCourseDays");
        String courseDays = "MONDAY, TUESDAY, WEDNESDAY";
        schedule.setCourseDays(courseDays);
        assertEquals("Incorrect course days were set", schedule.getCourseDays(), courseDays);
    }

    /**
     * Test of getStatus method, of class Schedule.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        String expResult = STATUS;
        String result = schedule.getStatus();
        assertEquals("The status was incorrect", expResult, result);
    }

    /**
     * Test of setStatus method, of class Schedule.
     */
    @Test
    public void testSetStatus() {
        System.out.println("setStatus");
        String status = "Declined";
        schedule.setStatus(status);
        assertEquals("The status was set incorrectly", schedule.getStatus(), status);
    }

    /**
     * Test of getIsDeleted method, of class Schedule.
     */
    @Test
    public void testGetIsDeleted() {
        System.out.println("getIsDeleted");
        boolean expResult = false;
        boolean result = schedule.getIsDeleted();
        assertFalse(result);

    }

    /**
     * Test of setIsDeleted method, of class Schedule.
     */
    @Test
    public void testSetIsDeleted() {
        System.out.println("setIsDeleted");
        boolean isDeleted = true;
        schedule.setIsDeleted(isDeleted);
        boolean result = schedule.getIsDeleted();
        assertTrue(result);

    }

    /**
     * Test of getCourse method, of class Schedule.
     */
    @Test
    public void testGetCourse() {
        System.out.println("getCourse");
        Course expResult = new Course(3);
        schedule.setCourse(expResult);
        Course result = schedule.getCourse();
        assertEquals("Course unexpectedly incorrect", expResult, result);

    }

    /**
     * Test of setCourse method, of class Schedule.
     */
    @Test
    public void testSetCourse() {
        System.out.println("setCourse");
        Course course = new Course(2);
        schedule.setCourse(course);
        assertNotNull("Shouldnt be null", schedule.getCourse());
        assertEquals("Course was set incorrect", schedule.getCourse(), course);
    }

    /**
     * Test of getInstructor method, of class Schedule.
     */
    @Test
    public void testGetInstructor() {
        System.out.println("getInstructor");
        Instructor expResult = new Instructor(1);
        schedule.setInstructor(expResult);
        Instructor result = schedule.getInstructor();
        assertEquals("The Instructor was set incorrectly", expResult, result);

    }

    /**
     * Test of setInstructor method, of class Schedule.
     */
    @Test
    public void testSetInstructor() {
        System.out.println("setInstructor");
        Instructor instructor = new Instructor(2);
        schedule.setInstructor(instructor);
        // TODO review the generated test code and remove the default call to fail.
        assertNotNull("Shouldn't be null", schedule.getInstructor());
        assertEquals("Incorrect Instructor", schedule.getInstructor(), instructor);
    }

    /**
     * Test of hashCode method, of class Schedule.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = 1;
        int result = schedule.hashCode();
        assertEquals("The hash code was incorrect", expResult, result);
        schedule = new Schedule();
        result = schedule.hashCode();
        expResult = 0;
        assertEquals("The has code was incorrect", expResult, result);

    }

    /**
     * Test of equals method, of class Schedule.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        boolean expResult = false;
        boolean result = schedule.equals(object);
        assertFalse(result);
        assertTrue(schedule.equals(schedule));

    }

    /**
     * Test of toString method, of class Schedule.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "bravo.models.Schedule[ scheduleId=1 ]";
        String result = schedule.toString();
        assertEquals("Unexpected ToString result", expResult, result);

    }
    
}
