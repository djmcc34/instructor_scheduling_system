/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author masud
 */
public class QualificationTest {
    Qualification qualification = null;
    Course course = null;
    final String DESCRIPTION = "A Test Course";
    final String SHORTTITLE = "Intro to a Test Course";
    final Integer ID = 1;
    final Integer CREDITHOURS = 4;
    final Integer COURSENUMBER = 111;
    final Integer DEPARTMENTID = 6;
    final String DEPARTMENTNAME = "Computer Science";
    final String DEPARTMENTSHORTNAME = "COMP";
    final boolean ISDELETED = false;
    Instructor instructor = null;
    final String FIRSTNAME = "John";
    final String MIDDLE = "W";
    final String LASTNAME = "Doe";
    final String STATUS = "Active";
    final String EMAIL = "John@Doe.com";
    final String PHONE = "724-550-1212";
    final Boolean ADJUNCT = true;
    
    public QualificationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instructor = new Instructor();
        instructor.setInstructorId(ID);
        instructor.setFirstName(FIRSTNAME);
        instructor.setMiddleInitial(MIDDLE);
        instructor.setLastName(LASTNAME);
        instructor.setEmail(EMAIL);
        instructor.setPhoneNumber(PHONE);
        instructor.setIsadjunct(ADJUNCT);
        instructor.setInstructorStatus(STATUS);
        course = new Course(ID, SHORTTITLE);
        course.setDescription(DESCRIPTION);
        course.setCreditHours(CREDITHOURS);
        course.setCourseNumber(COURSENUMBER);
        course.setDepartmentId(DEPARTMENTID);
        course.setDepartmentName(DEPARTMENTNAME);
        course.setDepartmentShortName(DEPARTMENTSHORTNAME);
        course.setIsDeleted(ISDELETED);
        qualification = new Qualification(1);
        qualification.setId(ID);
        qualification.setCourseId(course);
        qualification.setInstructorId(instructor);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Qualification.
     */
    @Test
    public void testGetQualificationId() {
        System.out.println("getQualificationId");
        Integer expResult = 1;
        Integer result = qualification.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Qualification.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        course.setId(3);
        Course result = course;
        qualification.setCourseId(course);
        assertEquals(qualification.getCourseId(),course);
    }

    /**
     * Test of getCourseId method, of class Qualification.
     */
    @Test
    public void testGetCourseId() {
        System.out.println("getCourseId");
        Integer expResult = 1;
        Course result = qualification.getCourseId();
        assertEquals(expResult, result.getId());
    }

    /**
     * Test of setCourseId method, of class Qualification.
     */
    @Test
    public void testSetCourseId() {
        System.out.println("setCourseId");
        course.setId(2);
        Integer expResult = 2;
        assertEquals(expResult, course.getId());
//        qualification.setCourseId(result);
    }

    /**
     * Test of getInstructorId method, of class Qualification.
     */
    @Test
    public void testGetInstructorId() {
        System.out.println("getInstructorId");
        Instructor result = qualification.getInstructorId();
        assertEquals(instructor, result);
    }

    /**
     * Test of setInstructorId method, of class Qualification.
     */
    @Test
    public void testSetInstructorId() {
        System.out.println("setInstructorId");
        instructor.setInstructorId(4);
        qualification.setInstructorId(instructor);
        assertEquals(instructor,qualification.getInstructorId());
    }

    /**
     * Test of equals method, of class Qualification.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        boolean expResult = false;
        boolean result = qualification.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Qualification.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "bravo.models.Qualifications[ id=1 ]";
        String result = qualification.toString();
        assertEquals(expResult, result);
    }
    
}
