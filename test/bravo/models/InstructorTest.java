/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joshua Lynn
 */
public class InstructorTest {
    Instructor instructor = null;
    final int ID = 1;
    final String FIRSTNAME = "John";
    final String MIDDLE = "W";
    final String LASTNAME = "Doe";
    final String STATUS = "Active";
    final String EMAIL = "John@Doe.com";
    final String PHONE = "724-550-1212";
    final Boolean ADJUNCT = true;
    
    public InstructorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instructor = new Instructor();
        instructor.setInstructorId(ID);
        instructor.setFirstName(FIRSTNAME);
        instructor.setMiddleInitial(MIDDLE);
        instructor.setLastName(LASTNAME);
        instructor.setEmail(EMAIL);
        instructor.setPhoneNumber(PHONE);
        instructor.setIsadjunct(ADJUNCT);
        instructor.setInstructorStatus(STATUS);
        
        
    }
    
    @After
    public void tearDown() {
        instructor = null;
    }

    /**
     * Test of Instructor Constructors
     */
    @Test
    public void testInstructorConstructors() {
        System.out.println("Testing Instructor Constructors");
        System.out.println("Testing Default Constructor");
        assertNotNull("Instructor should not be null", instructor);
        
        System.out.println("Testing Constructor with ID");
        instructor = new Instructor(ID);
        assertNotNull("Instructor should not be null", instructor);
        int result = instructor.getInstructorId();
        assertEquals("Incorrect ID", ID, result);
        
        System.out.println("Testing Constructor with ID, FirstName, MiddleInitial");
        instructor = new Instructor(ID, FIRSTNAME, MIDDLE);
        assertNotNull("Instructor should not be null", instructor);
        result = instructor.getInstructorId();
        String first = instructor.getFirstName();
        String middle = instructor.getMiddleInitial();
        assertEquals("Incorrect ID", ID, result);
        assertEquals("Incorrect First Name was Set", FIRSTNAME, first);
        assertEquals("Incorrect Middle Initial was Set", MIDDLE, middle);
        
    }
    
    
    
    /**
     * Test of getInstructorId method, of class Instructor.
     */
    @Test
    public void testGetInstructorId() {
        System.out.println("getInstructorId");
        Integer expResult = ID;
        Integer result = instructor.getInstructorId();
        assertEquals(expResult, result);

    }

    /**
     * Test of setInstructorId method, of class Instructor.
     */
    @Test
    public void testSetInstructorId() {
        System.out.println("setInstructorId");
        Integer expected = 5;
        instructor.setInstructorId(expected);
        Integer result = instructor.getInstructorId();
        assertEquals("Instructor ID incorrectly set", expected, result);
    }

    /**
     * Test of getFirstName method, of class Instructor.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        String expResult = FIRSTNAME;
        String result = instructor.getFirstName();
        assertEquals("Instructor First Name was set incorrectly", expResult, result);

    }

    /**
     * Test of setFirstName method, of class Instructor.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");
        String firstName = "Jane";
        instructor.setFirstName(firstName);
        String result = instructor.getFirstName();
        assertEquals("Instructor First Name set Incorrectly", firstName, result);
    }

    /**
     * Test of getMiddleInitial method, of class Instructor.
     */
    @Test
    public void testGetMiddleInitial() {
        System.out.println("getMiddleInitial");
        String expResult = MIDDLE;
        String result = instructor.getMiddleInitial();
        assertEquals("Instructor incorrect Middle Initial", expResult, result);

    }

    /**
     * Test of setMiddleInitial method, of class Instructor.
     */
    @Test
    public void testSetMiddleInitial() {
        System.out.println("setMiddleInitial");
        String middleInitial = "K";
        instructor.setMiddleInitial(middleInitial);
        String result = instructor.getMiddleInitial();
        assertEquals("Incorrect Instructor Middle Initial", middleInitial, result);
    }

    /**
     * Test of getLastName method, of class Instructor.
     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");
        String expResult = LASTNAME;
        String result = instructor.getLastName();
        assertEquals("Incorrect Last Name", expResult, result);

    }

    /**
     * Test of setLastName method, of class Instructor.
     */
    @Test
    public void testSetLastName() {
        System.out.println("setLastName");
        String lastName = "Lynn";
        instructor.setLastName(lastName);
        String result = instructor.getLastName();
        assertEquals("Incorrect Last Name", lastName, result);
    }

    /**
     * Test of getInstructorStatus method, of class Instructor.
     */
    @Test
    public void testGetInstructorStatus() {
        System.out.println("getInstructorStatus");
        String expResult = STATUS;
        String result = instructor.getInstructorStatus();
        assertEquals("Instructor Status is incorrect", expResult, result);

    }

    /**
     * Test of setInstructorStatus method, of class Instructor.
     */
    @Test
    public void testSetInstructorStatus() {
        System.out.println("setInstructorStatus");
        String instructorStatus = "Deleted";
        instructor.setInstructorStatus(instructorStatus);
        String result = instructor.getInstructorStatus();
        assertEquals("Incorrect Instructor Status", instructorStatus, result);
    }

    /**
     * Test of getEmail method, of class Instructor.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = EMAIL;
        String result = instructor.getEmail();
        assertEquals("Incorrect Instructor Email", expResult, result);

    }

    /**
     * Test of setEmail method, of class Instructor.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "test@test.com";
        instructor.setEmail(email);
        String result = instructor.getEmail();
        assertEquals("Incorrect Instructor Email", email, result);

    }

    /**
     * Test of getPhoneNumber method, of class Instructor.
     */
    @Test
    public void testGetPhoneNumber() {
        System.out.println("getPhoneNumber");
        String expResult = PHONE;
        String result = instructor.getPhoneNumber();
        assertEquals("Instructor Phone is Incorrect", expResult, result);

    }

    /**
     * Test of setPhoneNumber method, of class Instructor.
     */
    @Test
    public void testSetPhoneNumber() {
        System.out.println("setPhoneNumber");
        String phoneNumber = "724-867-5309";
        instructor.setPhoneNumber(phoneNumber);
        String result = instructor.getPhoneNumber();
        assertEquals("Incorrect Phone Number", phoneNumber, result);

    }

    /**
     * Test of getIsadjunct method, of class Instructor.
     */
    @Test
    public void testGetIsadjunct() {
        System.out.println("getIsadjunct");
        Boolean expResult = ADJUNCT;
        Boolean result = instructor.getIsadjunct();
        assertEquals("Status is not correct", expResult, result);

    }

    /**
     * Test of setIsadjunct method, of class Instructor.
     */
    @Test
    public void testSetIsadjunct() {
        System.out.println("setIsadjunct");
        Boolean isadjunct = false;
        instructor.setIsadjunct(isadjunct);
        Boolean result = instructor.getIsadjunct();
        assertFalse("Status was incorrectly set", result);

    }

    /**
     * Test of hashCode method, of class Instructor.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");

        int expResult = 1;
        int result = instructor.hashCode();
        assertEquals("Incorrect Hashcode", expResult, result);

    }

    /**
     * Test of equals method, of class Instructor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Instructor instance = new Instructor();
        boolean expResult = false;
        boolean result = instructor.equals(object);
        assertEquals(expResult, result);
        result = instructor.equals(instance);
        assertEquals(expResult, result);
        assertTrue("Same Object Should be the Same", instructor.equals(instructor));
    }

    /**
     * Test of toString method, of class Instructor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "bravo.models.Instructors[ instructorId=1 ]";
        String result = instructor.toString();
        assertEquals("Unexpected ToString Value", expResult, result);

    }
    
}
