/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Spaceman
 */
public class LoginControllerTest {
    
    RequestDispatcher rd;
    HttpServletResponse response;
    HttpServletRequest request;
    HttpSession session;
    
    String VIEW = "/authentication/login.jsp";
    
    public LoginControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        request = mock(HttpServletRequest.class);       //creating mock objects
        response = mock(HttpServletResponse.class);   
        rd = mock(RequestDispatcher.class);
        session = mock(HttpSession.class);
    }
    
    @After
    public void tearDown() {
        request = null;
        response = null;
        rd = null;
        session = null;
    }

    /**
     * Test of doGet method, of class LoginController. 
     * This method simulates what happens when a user tries to hit the login page 
     * without being authenticated, they should be sent to the login page. 
     * @throws java.lang.Exception
     */
    @Test
    public void testDoGetWithNoSession() throws Exception {
        when(request.getRequestDispatcher(VIEW)).thenReturn(rd);   //look at LoginController.java, it will call this when session is null, 
        new LoginController().doGet(request, response);           //if you do not return a request dispatcher there will be a nullpointer Exception
        verify(request, times(1)).getRequestDispatcher(VIEW);     //this is verifying that request.getRequestDispatcher(VIEW) was called only 1 time
        verify(rd, times(1)).forward(request, response);         //this is verifying that RequestDispatcher.forward(request, respsone) was called only 1 time
    }                                                              //look at line 37 of login controller to see where this happens ^

    /**
     * Test of doGet method, of class LoginController. 
     * This method simulates what happens when a user tries to hit the login page 
     * while already authenticated, they should be sent to the schedule page. 
     * @throws java.lang.Exception
     */
    @Test
    public void testDoGetWithSession() throws Exception {
        when(request.getSession(false)).thenReturn(session);      //you have to tell mockito to return something anytime your class would return something
        when(session.getAttribute("user")).thenReturn("user");    //else you will likely get a null pointer exception, or some other exception
        new LoginController().doGet(request, response);          //run the method so that we can see if appropriate methods were called
        verify(response, times(1)).sendRedirect("schedule");          
    }  
    
    /**
     * Test of doPost method with wrong password, of class LoginController.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPostWithWrongPass() throws Exception {
        when(request.getParameter("emailAddress")).thenReturn("test@test.com");
        when(request.getParameter("password")).thenReturn("notThePassword");
        when(request.getRequestDispatcher(VIEW)).thenReturn(rd);
        new LoginController().doPost(request, response);
        verify(request, times(1)).getRequestDispatcher(VIEW);     
        verify(rd, times(1)).forward(request, response); 
    }
    
    /**
     * Test of doPost method with good password, of class LoginController.
     * @throws java.lang.Exception
     */
    @Test
    public void testDoPostWithRightPass() throws Exception {
        when(request.getParameter("emailAddress")).thenReturn("test@test.com");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getSession(true)).thenReturn(session);
        new LoginController().doPost(request, response);  
        verify(response, times(1)).sendRedirect("schedule"); 
    }
    
}
