/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.models.Login;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.postgresql.core.NativeQuery;

/**
 *
 * @author Spaceman
 */
public class DBLoginTest {
    
    EntityManagerFactory emf;
    EntityManager em;
    Query nq;
    String QUERY = "SELECT * FROM login WHERE upper(email_address) = upper(?)";   // at the time of writing, this was the same query ebing executed by DBLogin class
    
    public DBLoginTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        emf = mock(EntityManagerFactory.class);
        em = mock(EntityManager.class);
        nq = mock(Query.class);
    }
    
    @After
    public void tearDown() {
        emf = null;
        em = null;
        nq = null;
    }

    /**
     * Test of getEntityManager method, of class DBLogin.
     */
    @Test
    public void testGetEntityManager() {
        EntityManagerFactory emfa = DBUtil.getEmFactory();
        DBLogin dbLog = new DBLogin(emfa);
        assertNotNull(dbLog.getEntityManager());
    }

    /**
     * Test of findLogin method, of class DBLogin.
     */
    @Test
    public void testFindLogin() throws Exception {
        LinkedList<Login> logList = new LinkedList<>();
        String emailAddress = "test@test.com";
        Login log1 = new Login();
        log1.setEmailAddress(emailAddress);
        log1.setPassword("password");
        logList.add(log1);
        DBLogin dbLog = new DBLogin(emf);
        when(emf.createEntityManager()).thenReturn(em);
        when(em.createNativeQuery(QUERY, Login.class)).thenReturn((Query) nq);
        when(nq.setParameter(1, emailAddress)).thenReturn(nq);
        when(nq.getResultList()).thenReturn(logList);
        List<Login> logins = dbLog.findLogin(emailAddress);
        assertEquals(logList.get(0), logins.get(0));
    }
    
}
