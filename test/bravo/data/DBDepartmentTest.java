/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;
import bravo.data.DBDepartment;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author masud
 */
public class DBDepartmentTest {
    
    public DBDepartmentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testGetDepartmentId(){
        System.out.println("getDepartmentId");
        DBDepartment instance = new DBDepartment();
        String compare = "";
        assertEquals( compare, instance);
        
    }
    
    @Test
    public void testGetDepartmentName(){
        System.out.println("getDepartmentName");
        DBDepartment instance = new DBDepartment();
        String literalResult = "";
        String result = instance.getDepartmentName();
        assertEquals(literalResult, result);
    }
    
    @Test
    public void testSetDepartmentName(){
        System.out.println("setDepartmentName");
        DBDepartment instance = new DBDepartment();
        String literalResult = "testName";
        instance.setDepartmentName("testName");
        String result = instance.setDepartmentName("testName");
        assertEquals(literalResult, result);
    }
    
    @Test
    public void testGetIsDeleted(){
        System.out.println("getIsDeleted");
        DBDepartment instance = new DBDepartment();
        boolean literalResult = false;
        boolean result = instance.getIsDeleted();
        assertEquals(literalResult, result);
    }
        
    @Test
    public void testSetIsDeleted(){
        System.out.println("setIsDeleted");
        DBDepartment instance = new DBDepartment();
        boolean literalResult = true;
        instance.setIsDeleted(true);
        boolean result = instance.getIsDeleted();
        assertEquals(literalResult, result);
    }
    
    @Test
    public void testGetShortName(){
        System.out.println("getShortName");
        DBDepartment instance = new DBDepartment();
        String literalResult = "";
        String result = instance.getShortName();
        assertEquals(literalResult, result);
    }
    
    @Test
    public void testSetShortName(){
        System.out.println("setShortName");
        String shortName = "";
        DBDepartment instance = new DBDepartment();
        instance.setShortName(shortName);
    }
}
