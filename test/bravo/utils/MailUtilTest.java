/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.utils;

import bravo.data.DBSchedule;
import bravo.data.DBUtil;
import bravo.models.Schedule;
import javax.persistence.EntityManagerFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joshua Lynn
 */
public class MailUtilTest {
    
    public MailUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sendConfirmationEmail method, of class MailUtil.
     */
    @Test
    public void testSendConfirmationEmail() throws Exception {
        System.out.println("sendConfirmationEmail");
        EntityManagerFactory factory = null;
        
        try {
            factory = DBUtil.getEmFactory();
            System.out.println("Got a factory");
            DBSchedule dbs = new DBSchedule(factory);
            System.out.println("Got a DBSchedule using factory");
            Schedule s = dbs.findSchedule(0);
            System.out.println("Found the requested schedule");
            MailUtil.sendConfirmationEmail(s);
            assertTrue("Test pass mail sent", true );
        }
        catch (Exception ex) {
            System.out.println(ex);
            fail("Error sending Mail");
        }

    }
    
}
