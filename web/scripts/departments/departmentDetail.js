$(document).ready(function () {
    $("#departmentForm").validate({
        rules: {
            departmentName: {
                required: true
            },
            shortName: {
                required: true
            }
        },
        messages: {
            departmentName: {
                required: 'Department Name is required.'
            },
            shortName: {
                required: 'Short Name is required.'
            }
        }
    });
});