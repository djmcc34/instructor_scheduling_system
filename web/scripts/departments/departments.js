$(function () {
    $('#department-list').DataTable({
        "aaSorting": [[0, 'asc']],
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [2]}
        ],
        "iDisplayLength": 50
    });

    $('.deleteDepartmentButton').click(function () {
        $('#departmentDeleteSuccessNotification').hide();

        //Set the department id on the modal before showing to use for deletion
        var departmentId = $(this).data('departmentId');
        $('#delete-modal #deleteModalDepartmentId').val(departmentId);
        $('#delete-modal').modal('show');
    });

    $('#delete-modal #deleteConfirmButton').click(function () {
        var departmentId = $('#delete-modal #deleteModalDepartmentId').val();

        $.ajax({
            url: 'departments?departmentId=' + departmentId,
            method: 'DELETE',
            success: function (data) {
                //$('#input').val(data.shortName + $('#number').val());
                
                
                var row = $('#row_' + departmentId);
                $('#department-list').dataTable().fnDeleteRow(row);

                $('#departmentDeletedSuccessNotification').show();
            },
            error: handleAjaxError
        });
    });
});