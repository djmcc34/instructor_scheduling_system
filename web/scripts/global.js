(function ($) {
    $.each(['show', 'hide'], function (i, ev) {
        var el = $.fn[ev];
        $.fn[ev] = function () {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });
})(jQuery);

$(function () {
    $('.alert').on('show', function () {
        $(this).delay(3000).fadeOut(800);
    });

    $('.alert:visible').delay(3000).fadeOut(800);
});

function handleAjaxError() {
    $('#unexpectedErrorNotification').show();
}