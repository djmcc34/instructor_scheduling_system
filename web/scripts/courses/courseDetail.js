var qualificationTemplate;

$(document).ready(function () {
    $("#courseForm").validate({
        rules: {
            courseNumber: {
                required: true,
                number: true,
                min: 1
            },
            title: {
                required: true
            },
            credits: {
                required: true,
                number: true,
                min: 1
            },
            departmentId: {
                required: true
            },
            description: {
                required: true
            }
        },
        messages: {
            courseNumber: {
                required: 'Course Number is required.',
                number: 'Course Number must be a valid number.',
                min: 'Course Number must be greater than 0.'
            },
            title: {
                required: 'Course Title is required.'
            },
            credits: {
                required: 'Course Credits are required.',
                number: 'Course Credits must be a valid number.',
                min: 'Course Credits must be greater than 0.'
            },
            departmentId: {
                required: 'Department must be selected.'
            },
            description: {
                required: 'A description must be given for this course.'
            }


        }
    });
    
    //Auto update department short name on department selection from dropdown box
    $('#departmentId').change(function () {
        var departmentId = $('#departmentId').val();
        var departmentValues = {};
        $("#departmentShortNames").children().each(function () {
            departmentValues[this.value] = this.innerHTML;
        });
        $('#departmentShortName').val(departmentValues[departmentId]);
    });
    
    
    //initialize the handlebars qualification template
    var qualificationTemplateSource = document.getElementById("qualification-template").innerHTML;
    qualificationTemplate = Handlebars.compile(qualificationTemplateSource);

    $('#addQualificationButton').click(function () {
        $('#addQualificationButton').hide();
        $('#spinner').show();

        $.ajax({
            url: 'api/instructor',
            method: 'GET',
            success: function (instructors) {
                var rowNumber = getNextQualificationNumber();
                var data = {
                    rowNumber: rowNumber,
                    instructors: instructors
                };

                var html = qualificationTemplate(data);
                $('#qualifications-list tbody').append(html);

                $('#addQualificationButton').show();
                $('#spinner').hide();
            },
            error: handleAjaxError
        });
    });

    //we need to fire the event at the document level since rows can be dynamically added
    $(document).on('click', '.deleteQualificationButton', function () {
        var qualificationId = $(this).data('qualificationId');

        if (qualificationId) {
            $('#deleteSuccessNotification').hide();

            $('#delete-qualification-modal #deleteModalQualificationId').val(qualificationId);
            $('#delete-qualification-modal').modal('show');
        } else {
            $(this).parents('tr').remove();
        }
    });

    $('#delete-qualification-modal #deleteConfirmButton').click(function () {
        var qualificationId = $('#delete-qualification-modal #deleteModalQualificationId').val();

        $.ajax({
            url: 'courseDetail?qualificationId=' + qualificationId,
            method: 'DELETE',
            success: function () {
                var row = $('#' + qualificationId);
                row.remove();
            },
            error: handleAjaxError
        });
    });
});

function getNextQualificationNumber() {
    var rows = $('#qualifications-list tbody tr');
    var maxRowNumber;

    $.each(rows, function (index, value) {
        var rowNum = $(this).data('rowNumber');
    });

    return $('#qualifications-list tbody tr').length;
};