$(function () {
    $('#course-list').dataTable();

    $('.deleteCourseButton').click(function () {
        $('#courseDeleteSuccessNotification').hide();

        //Set the course id on the modal before showing to use for deletion
        var courseId = $(this).data('courseId');
        $('#delete-modal #deleteModalCourseId').val(courseId);
        $('#delete-modal').modal('show');
    });

    $('#delete-modal #deleteConfirmButton').click(function () {
        var courseId = $('#delete-modal #deleteModalCourseId').val();

        $.ajax({
            url: 'courses?courseId=' + courseId,
            method: 'DELETE',
            success: function () {
                var row = $('#row_' + courseId);
                $('#course-list').dataTable().fnDeleteRow(row);
                
                $('#courseDeleteSuccessNotification').show();
            },
            error: handleAjaxError
        });
    });
});