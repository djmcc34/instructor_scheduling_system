var qualificationTemplate;

$(document).ready(function () {
    $("#instructorForm").validate({
        rules: {
            instructorFirstName: {
                required: true
            },
            instructorLastName: {
                required: true
            },
            instructorEmail: {
                required: true,
                email: true
            },
            instructorPhone: {
                required: true,
                phoneUS: true
            },
            instructorStatus: {
                required: true
            }
        },
        messages: {
            instructorFirstName: {
                required: 'First Name is required.'
            },
            instructorLastName: {
                required: 'Last Name is required.'
            },
            instructorEmail: {
                required: 'Email Address is required.',
                email: 'Please enter a valid email address.'
            },
            instructorPhone: {
                required: 'Phone Number is required.',
                phoneUS: 'Please enter a valid phone number.'
            },
            instructorStatus: {
                required: 'Status is required.'
            }
        }
    });

    //initialize the handlebars qualification template
    var qualificationTemplateSource = document.getElementById("qualification-template").innerHTML;
    qualificationTemplate = Handlebars.compile(qualificationTemplateSource);

    $('#addQualificationButton').click(function () {
        $('#addQualificationButton').hide();
        $('#spinner').show();

        $.ajax({
            url: 'api/courses',
            method: 'GET',
            success: function (courses) {
                var rowNumber = getNextQualificationNumber();
                var data = {
                    rowNumber: rowNumber,
                    courses: courses
                };

                var html = qualificationTemplate(data);
                $('#qualifications-list tbody').append(html);

                $('#addQualificationButton').show();
                $('#spinner').hide();
            },
            error: handleAjaxError
        });
    });

    //we need to fire the event at the document level since rows can be dynamically added
    $(document).on('click', '.deleteQualificationButton', function () {
        var qualificationId = $(this).data('qualificationId');

        if (qualificationId) {
            $('#deleteSuccessNotification').hide();

            $('#delete-qualification-modal #deleteModalQualificationId').val(qualificationId);
            $('#delete-qualification-modal').modal('show');
        } else {
            $(this).parents('tr').remove();
        }
    });

    $('#delete-qualification-modal #deleteConfirmButton').click(function () {
        var qualificationId = $('#delete-qualification-modal #deleteModalQualificationId').val();

        $.ajax({
            url: 'instructorDetail?qualificationId=' + qualificationId,
            method: 'DELETE',
            success: function () {
                var row = $('#' + qualificationId);
                row.remove();
            },
            error: handleAjaxError
        });
    });
});

function getNextQualificationNumber() {
    var rows = $('#qualifications-list tbody tr');
    var maxRowNumber

    $.each(rows, function (index, value) {
        var rowNum = $(this).data('rowNumber');
    });

    return $('#qualifications-list tbody tr').length;
}