$(function () {
    $('#instructor-list').DataTable({
        "aaSorting": [[0, 'asc']],
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [7]}
        ],
        "iDisplayLength": 50
    });

    $('.deleteInstructorButton').click(function () {
        $('#deleteSuccessNotification').hide();

        var instructorId = $(this).data('instructorId');
        $('#delete-modal #deleteModalInstructorId').val(instructorId);
        $('#delete-modal').modal('show');
    });

    $('#delete-modal #deleteConfirmButton').click(function () {
        var instructorId = $('#delete-modal #deleteModalInstructorId').val();

        $.ajax({
            url: 'instructors?instructorId=' + instructorId,
            method: 'DELETE',
            success: function () {
                var row = $('#row_' + instructorId);
                $('#instructor-list').dataTable().fnDeleteRow(row);
                
                $('#deleteSuccessNotification').show();
            },
            error: handleAjaxError
        });
    });
});