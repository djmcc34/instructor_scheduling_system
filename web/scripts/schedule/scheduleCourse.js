$(document).ready(function () {
    populateCourseDropDown();
    populateInstructorsDropDown();

    $('#departmentId').change(function () {
        populateCourseDropDown();
    });

    $('#courseId').change(function () {
        populateInstructorsDropDown();
    });


    $('.datepicker').datepicker({
    });

    jQuery.validator.addMethod("greaterThan", function (value, element, params) {

        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) > new Date($(params).val());
        }

        return isNaN(value) && isNaN($(params).val())
                || (Number(value) > Number($(params).val()));
    }, 'Must be greater than {0}.');

    $("#scheduleCourseForm").validate({
        rules: {
            departmentId: {
                required: true
            },
            courseId: {
                required: true
            },
            term: {
                required: true
            },
            year: {
                required: true
            },
            section: {
                required: true
            },
            location: {
                required: true
            },
            instructorId: {
                required: true
            },
            status: {
                required: true
            },
            startDate: {
                required: true,
                date: true
            },
            endDate: {
                required: true,
                date: true,
                greaterThan: '#startDate'
            }
        },
        messages: {
            departmentId: {
                required: "Department is required."
            },
            courseId: {
                required: "Course is required."
            },
            term: {
                required: "Term is required."
            },
            year: {
                required: "Year is required."
            },
            section: {
                required: "Section is required."
            },
            location: {
                required: "Location is required."
            },
            instructorId: {
                required: true
            },
            status: {
                required: true
            },
            startDate: {
                required: "Start Date is required.",
                date: "Start Date must be a valid date."
            },
            endDate: {
                required: "End Date is required.",
                date: "End Date must be a valid date.",
                greaterThan: "End Date must be later than Start Date."
            }
        }
    });
});

function populateCourseDropDown() {
    if ($('#departmentId').val()) {
        $.ajax({
            url: 'api/courses?departmentId=' + $('#departmentId').val(),
            method: 'GET',
            success: function (courses) {
                //populate the course drop down
                $('#courseId').empty();

                $('#courseId').append('<option value="">Select a Course</option');

                $.each(courses, function (i, v) {
                    $('#courseId').append('<option value="' + v.courseId + '">' + v.text + '</option');
                });
            },
            error: handleAjaxError
        });
    }
}

function populateInstructorsDropDown() {
    if ($('#courseId').val()) {
        $.ajax({
            url: 'api/qualifiedInstructors?courseId=' + $('#courseId').val(),
            method: 'GET',
            success: function (instructors) {
                //populate the instructors drop down
                $('#instructorId').empty();

                $('#instructorId').append('<option value="">Select an Instructor</option');

                $.each(instructors, function (i, v) {
                    $('#instructorId').append('<option value="' + v.instructorId + '">' + v.fullName + '</option');
                });
            },
            error: handleAjaxError
        });
    }
}