$(function () {
    $('#schedule-list').DataTable({
        "aaSorting": [[0, 'asc']],
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [11]}
        ],
        "iDisplayLength": 50
    });

    $('#departmentSelect').change(function () {
        window.location = getUrl();
    });

    $('#termSelect').change(function () {
        window.location = getUrl();
    });

    $('#scheduleCourseButton').click(function () {
        //pass the selected department and term to the schedule course controller
        var params = {};

        if ($('#departmentSelect').val()) {
            params.departmentId = $('#departmentSelect').val();
        }

        if ($('#termSelect').val()) {
            params.term = $('#termSelect').val();
        }

        var url = 'scheduleCourse?' + $.param(params);
        window.location = url;
    });

    $('.deleteScheduleButton').click(function () {
        $('#scheduleDeleteSuccessNotification').hide();

        //Set the department id on the modal before showing to use for deletion
        var scheduleId = $(this).data('scheduleId');
        $('#delete-modal #deleteModalScheduleId').val(scheduleId);
        $('#delete-modal').modal('show');
    });

    $('#delete-modal #deleteConfirmButton').click(function () {
        var scheduleId = $('#delete-modal #deleteModalScheduleId').val();

        $.ajax({
            url: 'schedule?scheduleId=' + scheduleId,
            method: 'DELETE',
            success: function () {
                var row = $('#row_' + scheduleId);
                $('#schedule-list').dataTable().fnDeleteRow(row);

                $('#scheduleDeletedSuccessNotification').show();
            },
            error: handleAjaxError
        });
    });
});

function getUrl() {
    var params = {};

    if ($('#departmentSelect').val()) {
        params.departmentId = $('#departmentSelect').val();
    }

    if ($('#termSelect').val()) {
        params.term = $('#termSelect').val();
    }

    return 'schedule?' + $.param(params);
}