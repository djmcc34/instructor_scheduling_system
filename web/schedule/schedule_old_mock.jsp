<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Schedule
    </jsp:attribute>

    <jsp:attribute name="styles">
        <style>
            .table {
                font-size: 80%;
                margin-bottom: 0;
            }

            .card-body {
                padding: 0;
            }
        </style>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="scripts/schedule/schedule.js"></script>
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-md-12">
                <h4 class="d-flex justify-content-between align-items-center font-weight-bold mb-2">
                    Schedule
                    <div>
                        <a href="scheduleCourse" class="btn btn-primary ">
                            <i class="far fa-calendar-alt"></i>&nbsp; Schedule Course
                        </a>
                        <a href="exportSchedule" class="btn btn-secondary">
                            <i class="fas fa-cloud-download-alt"></i>&nbsp; Export Schedule
                        </a>
                    </div>
                </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <select class="custom-select col-md-12 mb-2">
                    <option>Computer Science</option>
                </select>
            </div>
            <div class="col-md-3">
                <select class="custom-select col-md-12 mb-2">
                    <option>Fall 2018</option>
                    <option>Spring 2019</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div><b>12 Week Section U</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr class="alert-success">
                                    <td>COMP394</td>
                                    <td>CS Practicum 2</td>
                                    <td>Brian Gorman</td>
                                    <td>Instr. Approved</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="alert-warning">
                                    <td>COMP321</td>
                                    <td>Application Server Programming</td>
                                    <td>Tim Kington</td>
                                    <td>Pending</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <div><b>12 Week Section R</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr class="alert-success">
                                    <td>COMP394</td>
                                    <td>CS Practicum 2</td>
                                    <td>Brian Gorman</td>
                                    <td>Instr. Approved</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="alert-warning">
                                    <td>COMP321</td>
                                    <td>Application Server Programming</td>
                                    <td>Tim Kington</td>
                                    <td>Pending</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-md-4 pl-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>12 Week Section Q</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr class="alert-success">
                                    <td>COMP394</td>
                                    <td>CS Practicum 2</td>
                                    <td>Brian Gorman</td>
                                    <td>Instr. Approved</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="alert-warning">
                                    <td>COMP321</td>
                                    <td>Application Server Programming</td>
                                    <td>Tim Kington</td>
                                    <td>Pending</td>
                                    <td>
                                        <a href="javascript:void(0)" title="Edit Scheduled Course">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="deleteScheduledcourseButton" data-scheduled-course-id="$" style="margin-left:10px;" title="Delete Scheduled Course">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>6 Week Section E</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 pl-0 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>6 Week Section F</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 pl-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>6 Week Section H</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 2</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-0 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 3</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-0 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 4</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-0 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 5</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-0 pr-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 6</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2 pl-0">
                <div class="card">
                    <div class="card-header">
                        <div><b>3 Week Section 7</b></div>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>COMP394</td>
                                    <td>Brian Gorman</td>
                                </tr>
                                <tr>
                                    <td>COMP321</td>
                                    <td>Tim Kington</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </jsp:body>
</t:layout>