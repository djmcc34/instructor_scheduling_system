<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Schedule
    </jsp:attribute>

    <jsp:attribute name="styles">
        <!--Include other 3rd party styles that don't need to be on every page-->
        <link rel="stylesheet" href="styles/lib/datatables/datatables.css">
        <style>
            .table {
                font-size: 80%;
                margin-bottom: 0;
            }

            .card-body {
                padding: 0;
            }


        </style>
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <!--Include other 3rd party scripts that don't need to be on every page-->
        <script src="scripts/lib/datatables/datatables.js"></script>
        <script src="scripts/schedule/schedule.js"></script>
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-md-12">
                <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
                    Schedule
                    <div>
                        <a href="javascript:void(0)" class="btn btn-primary" id="scheduleCourseButton">
                            <i class="far fa-calendar-alt"></i>&nbsp; Schedule Course
                        </a>
                        <a href="exportSchedule" class="btn btn-secondary">
                            <i class="fas fa-cloud-download-alt"></i>&nbsp; Export Schedule
                        </a>
                    </div>
                </h4>
            </div>
        </div>

        <div class="alert alert-success alert-dismissible" id="scheduleDeletedSuccessNotification" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Scheduled Course deleted successfully!
        </div>

        <div class="row mb-2">
            <div class="col-md-4">
                <select class="custom-select" id="departmentSelect">
                    <option value="">All</option>
                    <c:forEach items="${departments}" var="department">
                        <option value="${department.departmentId}" ${department.departmentId == selectedDepartmentId ? 'selected' : ''}>${department.shortName} - ${department.departmentName}</option>
                    </c:forEach>
                </select>   
            </div>
            <div class="col-md-4">
                <select class="custom-select" id="termSelect">
                    <option value="">All</option>
                    <c:forEach items="${terms}" var="term">
                        <option value="${term}" ${term == selectedTerm ? 'selected' : ''}>${term}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="card">
            <div class="card-datatable table-responsive">
                <table id="schedule-list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Course</th>
                            <th>Term</th>
                            <th>Section</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Days of the Week</th>
                            <th>Instructor</th>
                            <th>Status</th>
                            <th>Location</th>
                            <th style="width:5em;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${scheduleEntities}" var="sched">
                            <tr id="row_${sched.scheduleId}">
                                <td>${sched.getFormattedCourseShortName()}</td>
                                <td>${sched.getFormattedTerm()}</td>
                                <td>${sched.section}</td>
                                <td data-sort="${sched.startDate}">${sched.getFormattedStartDate()}</td>
                                <td data-sort="${sched.endDate}">${sched.getFormattedEndDate()}</td>
                                <td data-sort="${sched.startTime}">${sched.getFormattedStartTime()}</td>
                                <td data-sort="${sched.endTime}">${sched.getFormattedEndTime()}</td>
                                <td>${sched.daysOfWeek}</td>
                                <td>${sched.getFormattedInstructorName()}</td>
                                <td>${sched.status}</td>
                                <td>${sched.location}</td>
                                <td>
                                    <a href="scheduleCourse?scheduleId=${sched.scheduleId}" title="Edit Schedule">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="deleteScheduleButton" data-schedule-id="${sched.scheduleId}" style="margin-left:10px;" title="Delete Instructor">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="delete-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Scheduled Course
                        </h5>
                        <input type="hidden" id="deleteModalScheduleId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this scheduled course?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
        </div>

    </jsp:body>
</t:layout>