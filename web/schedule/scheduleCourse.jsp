<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Schedule course
    </jsp:attribute>

    <jsp:attribute name="styles">
        <link rel="stylesheet" href="scripts/lib/bootstrap-datepicker-1.8.0/css/bootstrap-datepicker.min.css" />
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="scripts/lib/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="scripts/lib/jquery-validation/dist/additional-methods.min.js"></script>
        <script src="scripts/lib/bootstrap-datepicker-1.8.0/js/bootstrap-datepicker.min.js"></script>
        <script src="scripts/schedule/scheduleCourse.js"></script>
    </jsp:attribute>

    <jsp:body>
        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Schedule Course</div>
        </h4>

        <div class="card mb-4">
            <h6 class="card-header">
                Schedule Info
            </h6>
            <div class="card-body">
                <form action="scheduleCourse" method="POST" id="scheduleCourseForm">
                    <input type="hidden" id="scheduleId" name="scheduleId" value="${scheduleId}" />
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Department</label>
                            <c:choose>
                                <c:when test="${scheduleId == null}">
                                    <select class="custom-select col-md-6" id="departmentId" name="departmentId" ${scheduleId == null ? "" : "disabled"}>
                                        <option value="">Select a Department</option>
                                        <c:forEach items="${departments}" var="department">
                                            <option value="${department.departmentId}" ${department.departmentId == selectedDepartmentId ? 'selected' : ''}>${department.shortName} - ${department.departmentName}</option>
                                        </c:forEach>
                                    </select>  
                                </c:when>
                                <c:otherwise>
                                    <select class="custom-select col-md-6" disabled>
                                        <option>${department.shortName} - ${department.departmentName}</option>
                                    </select>
                                </c:otherwise>
                            </c:choose>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="form-label">Course</label>
                            <c:choose>
                                <c:when test="${scheduleId == null}">
                                    <select class="custom-select col-md-6" id="courseId" name="courseId">
                                        <option value="">Select a Course</option>
                                    </select>
                                </c:when>
                                <c:otherwise>
                                    <select class="custom-select col-md-6" disabled>
                                        <option>${department.shortName}${course.courseNumber} - ${course.shortTitle}</option>
                                    </select>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">Term</label>
                            <select class="custom-select" id="term" name="term">
                                <option value="">Select a Term</option>
                                <c:forEach items="${terms}" var="term">
                                    <option value="${term}" ${schedule.term == term ? 'selected' : ''}>${term}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">&nbsp;</label>
                            <select class="custom-select" id="year" name="year">
                                <option value="">Select a Year</option>
                                <c:forEach items="${years}" var="year">
                                    <option value="${year}" ${schedule.year == year ? 'selected' : ''}>${year}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">Section</label>
                            <input type="text" class="form-control" placeholder="Section" id="section" name="section" value="${schedule.section}">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">Location</label>
                            <select class="custom-select" id="location" name="location">
                                <option value="">Select a Location</option>
                                <c:forEach items="${locations}" var="location">
                                    <option value="${location}" ${schedule.location == location ? 'selected' : ''}>${location}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">Instructor</label>
                            <select class="custom-select" id="instructorId" name="instructorId">
                                <c:choose>
                                    <c:when test="${scheduleId == null}">
                                        <option value="">Select an Instructor</option>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach items="${qualifiedInstructors}" var="instructor">
                                            <option value="${instructor.instructorId}" ${instructor.instructorId == schedule.instructor.instructorId ? 'selected' : ''}>${instructor.firstName} ${instructor.lastName}</option>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">Status</label>
                            <select class="custom-select" id="status" name="status">
                                <option value="">Select a Status</option>
                                <c:forEach items="${statuses}" var="status">
                                    <option value="${status}" ${schedule.status == status ? 'selected' : ''}>${status}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">Start Date</label>
                            <input type="text" class="form-control datepicker" placeholder="Start Date" id="startDate" name="startDate" value="${formattedStartDate}">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">End Date</label>
                            <input type="text" class="form-control datepicker" placeholder="End Date" id="endDate" name="endDate" value="${formattedEndDate}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">Start Time</label>
                            <input type="time" class="form-control timepicker" placeholder="Start Time" id="startTime" name="startTime" value="${formattedStartTime}">
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">End Time</label>
                            <input type="time" class="form-control timepicker" placeholder="End Time" id="endTime" name="endTime" value="${formattedEndTime}">
                        </div>
                    </div>

                    <h6>Course Days</h6>
                    <div class="form-row mb-4 pl-2">
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input" name="Sunday" ${isSunday ? "checked" : ""}>
                            <span class="custom-control-label">Sunday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input" name="Monday" ${isMonday ? "checked" : ""}>
                            <span class="custom-control-label">Monday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input"  name="Tuesday" ${isTuesday ? "checked" : ""}>
                            <span class="custom-control-label">Tuesday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input" name="Wednesday" ${isWednesday ? "checked" : ""}>
                            <span class="custom-control-label">Wednesday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input" name="Thursday" ${isThursday ? "checked" : ""}>
                            <span class="custom-control-label">Thursday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0 mr-3">
                            <input type="checkbox" class="custom-control-input" name="Friday" ${isFriday ? "checked" : ""}>
                            <span class="custom-control-label">Friday</span>
                        </label>
                        <label class="custom-control custom-checkbox m-0">
                            <input type="checkbox" class="custom-control-input" name="Saturday" ${isSaturday ? "checked" : ""}>
                            <span class="custom-control-label">Saturday</span>
                        </label>
                    </div>

                    <a href="schedule" class="btn btn-secondary">Cancel</a>
                    <input type="submit" class="btn btn-primary ml-2" value="Save" />

            </div>
        </form>

    </jsp:body>
</t:layout>