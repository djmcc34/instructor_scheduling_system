<!DOCTYPE html>

<html lang="en" class="default-style">

    <head>
        <title>Login - ISS</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/x-icon" href="favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="styles/lib/fonts/fontawesome.css">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="styles/lib/bootstrap/bootstrap.css" class="theme-settings-bootstrap-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/appwork.css" class="theme-settings-appwork-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/theme-corporate.css" class="theme-settings-theme-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/colors.css" class="theme-settings-colors-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/uikit.css">
        <link rel="stylesheet" href="styles/lib/appwork-template/demo.css">

        <script src="scripts/lib/appwork-template/material-ripple.js"></script>
        <script src="scripts/lib/appwork-template/layout-helpers.js"></script>

        <!-- Theme settings -->
        <!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
        <script src="scripts/lib/appwork-template/theme-settings.js"></script>

        <!-- Core scripts -->
        <script src="scripts/lib/appwork-template/pace.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <link rel="stylesheet" href="styles/global.css"/>

        <!-- Page -->
        <link rel="stylesheet" href="styles/authentication/login.css">
    </head>

    <body>
        <div class="authentication-wrapper authentication-1 px-4">
            <div class="authentication-inner py-5">
                <div class="alert alert-danger" role="alert" ${empty loginError ? "hidden" : ""}>
                    <strong>Login Error:</strong> Username or Password Incorrect
                </div>
                <h4 class="text-center">Instructor Scheduling System</h4>
                <!-- Form -->
                <form class="my-5" id="loginForm" action="login" method="POST">
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input type="text" class="form-control" id="emailAddress" name="emailAddress">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" />
                    </div>
                    <div class="d-flex justify-content-between align-items-center m-0">
                        <label class="custom-control custom-checkbox m-0">
                            <input type="checkbox" class="custom-control-input">
                            <span class="custom-control-label">Remember me</span>
                        </label>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </form>
                <!-- / Form -->
                <!--
                <div class="text-center text-muted">
                    Don't have an account yet?
                    <a href="javascript:void(0)">Sign Up</a>
                </div>
                -->
            </div>
        </div>
        <!-- Core scripts -->
        <script src="scripts/lib/popper/popper.js"></script>
        <script src="scripts/lib/bootstrap/bootstrap.js"></script>
        <script src="scripts/lib/appwork-template/sidenav.js"></script>
        <!-- Demo -->
        <script src="scripts/lib/appwork-template/demo.js"></script>
        <script src="scripts/global.js"></script>
        <script src="scripts/lib/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="scripts/authentication/login.js"></script>
    </body>
</html>