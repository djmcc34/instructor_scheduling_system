<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="pageTitle" fragment="true" %>
<%@attribute name="styles" fragment="true" %>
<%@attribute name="scripts" fragment="true" %>

<!DOCTYPE html>
<html lang="en" class="default-style">
    <head>
        <title><jsp:invoke fragment="pageTitle"/> - ISS</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/x-icon" href="favicon.ico">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="styles/lib/fonts/fontawesome.css">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="styles/lib/bootstrap/bootstrap.css" class="theme-settings-bootstrap-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/appwork.css" class="theme-settings-appwork-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/theme-corporate.css" class="theme-settings-theme-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/colors.css" class="theme-settings-colors-css">
        <link rel="stylesheet" href="styles/lib/appwork-template/uikit.css">
        <link rel="stylesheet" href="styles/lib/appwork-template/demo.css">

        <script src="scripts/lib/appwork-template/material-ripple.js"></script>
        <script src="scripts/lib/appwork-template/layout-helpers.js"></script>

        <!-- Theme settings -->
        <!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
        <script src="scripts/lib/appwork-template/theme-settings.js"></script>

        <!-- Core scripts -->
        <script src="scripts/lib/appwork-template/pace.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <link rel="stylesheet" href="styles/global.css"/>
        <jsp:invoke fragment="styles"/>
    </head>

    <body>

        <!-- Layout wrapper -->
        <div class="layout-wrapper layout-2">
            <div class="layout-inner">

                <!-- Layout sidenav -->
                <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-dark">

                    <!-- Brand demo (see assets/css/demo/demo.css) -->
                    <div class="app-brand demo">
                        <span class="app-brand-logo demo bg-primary">
                            <svg viewBox="0 0 148 80" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <defs>
                            <linearGradient id="a" x1="46.49" x2="62.46" y1="53.39" y2="48.2" gradientUnits="userSpaceOnUse">
                            <stop stop-opacity=".25" offset="0"></stop>
                            <stop stop-opacity=".1" offset=".3"></stop>
                            <stop stop-opacity="0" offset=".9"></stop>
                            </linearGradient>
                            <linearGradient id="e" x1="76.9" x2="92.64" y1="26.38" y2="31.49" xlink:href="#a"></linearGradient>
                            <linearGradient id="d" x1="107.12" x2="122.74" y1="53.41" y2="48.33" xlink:href="#a"></linearGradient>
                            </defs>
                            <path style="fill: #fff;" transform="translate(-.1)" d="M121.36,0,104.42,45.08,88.71,3.28A5.09,5.09,0,0,0,83.93,0H64.27A5.09,5.09,0,0,0,59.5,3.28L43.79,45.08,26.85,0H.1L29.43,76.74A5.09,5.09,0,0,0,34.19,80H53.39a5.09,5.09,0,0,0,4.77-3.26L74.1,35l16,41.74A5.09,5.09,0,0,0,94.82,80h18.95a5.09,5.09,0,0,0,4.76-3.24L148.1,0Z"></path>
                            <path transform="translate(-.1)" d="M52.19,22.73l-8.4,22.35L56.51,78.94a5,5,0,0,0,1.64-2.19l7.34-19.2Z"
                                  fill="url(#a)"></path>
                            <path transform="translate(-.1)" d="M95.73,22l-7-18.69a5,5,0,0,0-1.64-2.21L74.1,35l8.33,21.79Z"
                                  fill="url(#e)"></path>
                            <path transform="translate(-.1)" d="M112.73,23l-8.31,22.12,12.66,33.7a5,5,0,0,0,1.45-2l7.3-18.93Z"
                                  fill="url(#d)"></path>
                            </svg>
                        </span>
                        <a href="schedule" class="app-brand-text demo sidenav-text font-weight-normal ml-2">ISS</a>
                    </div>

                    <div class="sidenav-divider mt-0"></div>

                    <!-- Links -->
                    <ul class="sidenav-inner py-1">

                        <li class="sidenav-item">
                            <a href="schedule" class="sidenav-link">
                                <i class="sidenav-icon far fa-calendar-alt d-block"></i>
                                <div>Schedule</div>
                            </a>
                        </li>

                        <li class="sidenav-item">
                            <a href="courses" class="sidenav-link">
                                <i class="sidenav-icon fas fa-chalkboard d-block"></i>
                                <div>Courses</div>
                            </a>
                        </li>

                        <li class="sidenav-item">
                            <a href="instructors" class="sidenav-link">
                                <i class="sidenav-icon fas fa-user-friends d-block"></i>
                                <div>Instructors</div>
                            </a>
                        </li>

                        <li class="sidenav-item">
                            <a href="departments" class="sidenav-link">
                                <i class="sidenav-icon far fa-building d-block"></i>
                                <div>Departments</div>
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- / Layout sidenav -->
                <!-- Layout container -->
                <div class="layout-container">
                    <!-- Layout content -->
                    <div class="layout-content">
                        <!-- Content -->
                        <div class="container-fluid flex-grow-1 container-p-y">
                            <div class="alert alert-danger alert-dismissible" id="unexpectedErrorNotification" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                An unexpected error has occurred, please try again or contact support if the problem persists.
                            </div>

                            <c:if test="${not empty alertMessage}">
                                <div class="alert ${alertClass} alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    ${alertMessage}
                                </div>

                                <%
                                    //Clear out error message so it's not displayed on other pages
                                    session.setAttribute("alertMessage", "");
                                %>
                            </c:if>

                            <jsp:doBody/>
                        </div>
                        <!-- / Content -->
                    </div>
                    <!-- / Content -->
                </div>
                <!-- Layout content -->
            </div>
            <!-- / Layout container -->
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
        <!-- / Layout wrapper -->

        <!-- Core scripts -->
        <script src="scripts/lib/popper/popper.js"></script>
        <script src="scripts/lib/bootstrap/bootstrap.js"></script>
        <script src="scripts/lib/appwork-template/sidenav.js"></script>

        <!-- Demo -->
        <script src="scripts/lib/appwork-template/demo.js"></script>

        <script src="scripts/global.js"></script>

        <jsp:invoke fragment="scripts"/>
    </body>

</html>