<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Department Info
    </jsp:attribute>

    <jsp:attribute name="styles">
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="scripts/lib/jquery-validation/dist/jquery.validate.js"></script>
        <script src="scripts/departments/departmentDetail.js"></script>
    </jsp:attribute>

    <jsp:body>
        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Edit Department</div>
        </h4>

        <div class="card mb-4">
            <h6 class="card-header">
                Department Info
            </h6>
            <div class="card-body">
                <form action="departmentDetail" method="POST" id="departmentForm">
                    <input type="hidden" id="departmentId" name="departmentId" value="${departmentId}"/>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="form-label">Name</label>
                            <input id="departmentName" name="departmentName" type="text" class="form-control col-md-6" placeholder="Name (Computer Science, etc.)" value="${departmentName}" autocomplete="off">
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Short Name</label>
                            <input id="shortName" name="shortName" type="text" class="form-control col-md-6" placeholder="Short Name (COMP, etc.)" value="${shortName}" autocomplete="off">
                        </div>
                    </div>
                    <a href="departments" class="btn btn-secondary">Cancel</a>
                    <input type="submit" class="btn btn-primary" value="Save">
                </form>
            </div>
        </div>
    </jsp:body>
</t:layout>