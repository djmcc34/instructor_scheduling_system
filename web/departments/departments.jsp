<%@ page language="java" contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Departments
    </jsp:attribute>

    <jsp:attribute name="styles">
        <!--Include other 3rd party styles that don't need to be on every page-->
        <link rel="stylesheet" href="styles/lib/datatables/datatables.css">
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <!--Include other 3rd party scripts that don't need to be on every page-->
        <script src="scripts/lib/datatables/datatables.js"></script>
        <script src="scripts/departments/departments.js"></script>
    </jsp:attribute>

    <jsp:body>
        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Departments</div>
            <a href="departmentDetail" class="btn btn-primary btn-round d-block">
                <i class="fa fa-plus"></i>&nbsp; Create Department
            </a>
        </h4>

        <div class="alert alert-success alert-dismissible" id="departmentDeletedSuccessNotification" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">�</button>
            Department deleted successfully!
        </div>

        <div class="card">
            <div class="card-datatable table-responsive">
                <table id="department-list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Department Name</th>
                            <th>Short Name</th>
                            <th style="width:5em;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${departments}" var="department">
                            <tr  id="row_${department.departmentId}">
                                <td>${department.departmentName}</td>
                                <td>${department.shortName}</td>
                                <td>
                                    <a href="departmentDetail?departmentId=${department.departmentId}" title="Edit Department">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="deleteDepartmentButton" data-department-id="${department.departmentId}" style="margin-left:10px;" title="Delete Department">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="delete-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Department
                        </h5>
                        <input type="hidden" id="deleteModalDepartmentId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">�</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this department?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
        </div>
    </jsp:body>
</t:layout>