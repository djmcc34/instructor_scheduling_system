<%@ page language="java" contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>



<t:layout>
    <jsp:attribute name="pageTitle">
        Instructor Confirmation
    </jsp:attribute>

    <jsp:attribute name="styles">
    </jsp:attribute>

    <jsp:body>        

        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Please Complete Confirmation Below</div>
        </h4>

        <div class="card mb-4">
            <h6 class="card-header">

            </h6>
            <c:if test="${valid}">
                <div class="card-body">
                    Hello ${name},<br><br>
                    You have been selected to be an instructor at Franklin University.<br><br>
                    
                    Please indicate by clicking the buttons below if you will Accept or Decline this teaching opportunity.<br>
                    <b>Course Information:</b><br>
                    <b>Course Number: </b>${cnumber}<br>
                    <b>Course Name: </b>${cname}<br><br>
                    
                    
                    To complete the schedule for this course please accept or decline this teaching opportunity at Franklin University.
                    <form action="confirmation" method="POST" id="confirmationForm">
                        <input type="hidden" id="scheduleId" name="scheduleId" value="${scheduleId}"/>
                        <input type="submit" id= "accepted" name="accepted" class="btn btn-primary" value="Accept">
                        <input type="submit" class="btn btn-secondary" value="Decline">
                    </form>
                </div>
            </c:if>
        </div>        
    </jsp:body>
</t:layout>
