<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="rowNumber" value="0" scope="page" />

<t:layout>
    <jsp:attribute name="pageTitle">
        Course Info
    </jsp:attribute>

    <jsp:attribute name="styles">
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="scripts/lib/handlebars/handlebars.js"></script>
        <script src="scripts/lib/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="scripts/courses/courseDetail.js"></script>
        
        <!--handlebars template for qualifications-->
        <!-- this will allow us to dynamically add rows to the qualifications table from the javascript -->
        <script id="qualification-template" type="text/x-handlebars-template">
            <tr  id="" data-row-number="{{rowNumber}}">
                <td> 
                    <!-- naming these selects the same will allow us to get an array of values on the server -->
                    <select class="custom-select" id="qualificationInstructorIds" name="qualificationInstructorIds">
                        <option value="">Select Instructor</option>
                        {{#each instructors}}
                            <option value="{{instructorId}}">{{fullName}}</option>
                        {{/each}}
                    </select>
                </td>
                <td>
                    <a href="javascript:void(0)" class="deleteQualificationButton" data-instructor-id="" style="margin-left:10px;" title="Delete Qualification">
                        <i class="far fa-trash-alt"></i>
                    </a>                                                
                </td>
            </tr>
        </script>
    </jsp:attribute>

    <jsp:body>
        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Edit Course</div>
        </h4>

        <div class="card mb-4">
            <h6 class="card-header">
                Course Info
            </h6>
            <div class="card-body">
                <form action="courseDetail" method="POST" id="courseForm">
                    <input type="hidden" id="courseId" name="courseId" value="${courseId}"/>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="form-label">Department</label>
                            <select class="custom-select" id="departmentId" name="departmentId">
                                <option value="">Select a Department</option>
                                <c:forEach items="${departments}" var="dept">
                                    <option value="${dept.departmentId}" ${dept.departmentId == departmentId ? 'selected' : ''}>${dept.departmentName}</option>
                                </c:forEach>
                            </select>
                            <select class="hidden-select" id="departmentShortNames" name="departmentShortNames" hidden>
                                <option value="">Select a Department</option>
                                <c:forEach items="${departments}" var="dept">
                                    <option value="${dept.departmentId}" hidden> ${dept.shortName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label class="form-label">&nbsp</label>
                            <input type="text" class="form-control" placeholder="Department Short Name" id="departmentShortName" name="departmentShortName" value="${deptShortName}" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="form-label">Course Number</label>
                            <input type="text" class="form-control" placeholder="Name" id="courserNumber" name="courseNumber" value="${courseNumber}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Title</label>
                        <input type="text" class="form-control col-md-6" placeholder="Title" id="title" name="title" value="${title}">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Description</label>
                        <textarea class="form-control col-md-6" placeholder="Description" id="description" name="description">${description}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Credits</label>
                        <input type="text" class="form-control col-md-6" placeholder="Credits" id="credits" name="credits" value="${credits}">
                    </div>

                    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold">
                        <div>Instructors Qualified to Teach Course</div>
                    </h4>    
                    <div class="row">
                        <div class="col-md-6 table-responsive">
                            <table  id="qualifications-list" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Instructor Name</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${qualifications}" var="qualification">
                                        <tr id="${qualification.id}" data-row-number="${rowNumber}">
                                            <td>${qualification.instructorName}</td>
                                            <td>
                                                <a href="javascript:void(0)" class="deleteQualificationButton" data-qualification-id="${qualification.id}" style="margin-left:10px;" title="Delete Qualification">
                                                    <i class="far fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <c:set var="rowNumber" value="${rowNumber + 1}" scope="page"/>
                                    </c:forEach>
                                </tbody>
                            </table>  
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-6 ">
                            <a href="javascript:void(0)"id="spinner" class="float-md-right" style="display:none;" ><i class="fa fa-spinner fa-spin"></i></a>
                            <a href="javascript:void(0)" class="float-md-right" id="addQualificationButton"><i class="fa fa-plus"></i> add instructor</a>
                        </div>
                    </div>
                    <a href="courses" class="btn btn-secondary">Cancel</a>
                    <input type="submit" class="btn btn-primary" value="Save">
                </form>
            </div>
        </div>
                    <div class="modal fade" id="delete-qualification-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Qualification
                        </h5>
                        <input type="hidden" id="deleteModalQualificationId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this qualification? The instructor will no longer be able to be scheduled for this course.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
    </jsp:body>
</t:layout>