<%@ page language="java" contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Courses
    </jsp:attribute>

    <jsp:attribute name="styles">
        <!--Include other 3rd party styles that don't need to be on every page-->
        <link rel="stylesheet" href="styles/lib/datatables/datatables.css">
        <link rel="stylesheet" href="styles/courses/courses.css" />
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <!--Include other 3rd party scripts that don't need to be on every page-->
        <script src="scripts/lib/datatables/datatables.js"></script>
        <script src="scripts/courses/courses.js"></script>
    </jsp:attribute>

    <jsp:body>

        <div class="row">
            <div class="col-md-12">
                <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
                    Schedule
                    <div class="btn-group">
                        <a href="courseDetail" class="btn btn-primary btn-round d-block">
                            <i class="fa fa-plus"></i>&nbsp; Create course
                        </a>
                        &nbsp;
                        <button type="button" class="btn btn-primary btn-round d-block" data-toggle="modal" data-target="#courseImportModal">
                            <i class="fas fa-cloud-upload-alt"></i>&nbsp; Import Course
                        </button>
                    </div>
                </h4>
            </div>
        </div>

        <div class="alert alert-success alert-dismissible" id="courseDeleteSuccessNotification" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">�</button>
            Course deleted successfully!
        </div>

        <div class="card">
            <div class="card-datatable table-responsive">
                <table id="course-list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Course Number</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Credit Hours</th>
                            <th>Department</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${courses}" var="course">
                            <tr  id="row_${course.id}">
                                <td>${course.departmentShortName}${course.courseNumber}</td>
                                <td>${course.shortTitle}</td>
                                <td>${course.description}</td>
                                <td>${course.creditHours}</td>
                                <td>${course.departmentName}</td>
                                <td>
                                    <a href="courseDetail?courseId=${course.id}" title="Edit Course">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="deleteCourseButton" data-course-id="${course.id}" style="margin-left:10px;" title="Delete Course">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="delete-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Course
                        </h5>
                        <input type="hidden" id="deleteModalCourseId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">�</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this course?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- Courses Import Modal -->
        <div class="modal fade" id="courseImportModal" tabindex="-1" role="dialog" aria-labelledby="courseImportModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="courseImportModal">Import Course</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <body>
                            <h3>File Upload:</h3>
                            Select a file to upload: <br />
                            <form action = "courseImport" method = "post"
                                  enctype = "multipart/form-data">
                                <input type = "file" name = "file" id="file"/>
                                <br />
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <input type="submit" class="btn btn-primary" value="Import">
                                </div>
                            </form>
                        </body>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>