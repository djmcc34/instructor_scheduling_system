<%@ page language="java" contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:layout>
    <jsp:attribute name="pageTitle">
        Instructors
    </jsp:attribute>

    <jsp:attribute name="styles">
        <!--Include other 3rd party styles that don't need to be on every page-->
        <link rel="stylesheet" href="styles/lib/datatables/datatables.css">
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <!--Include other 3rd party scripts that don't need to be on every page-->
        <script src="scripts/lib/datatables/datatables.js"></script>
        <script src="scripts/instructors/instructors.js"></script>
    </jsp:attribute>

    <jsp:body>
        <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
            <div>Instructors</div>
            <a href="instructorDetail" class="btn btn-primary btn-round d-block">
                <i class="fa fa-plus"></i>&nbsp; Create Instructor
            </a>
        </h4>

        <div class="alert alert-success alert-dismissible" id="deleteSuccessNotification" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">�</button>
            Instructor deleted successfully!
        </div>

        <div class="card">
            <div class="card-datatable table-responsive">
                <table id="instructor-list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>First</th>
                            <th>Middle</th>
                            <th>Last</th>
                            <th>Status</th>
                            <th>E-Mail</th>
                            <th>Phone</th>
                            <th>Adjunct</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${instructors}" var="instructor">
                            <tr  id="row_${instructor.getInstructorId()}">
                                <td>${instructor.firstName}</td>
                                <td>${instructor.middleInitial}</td>
                                <td>${instructor.lastName}</td>
                                <td>${instructor.instructorStatus}</td>
                                <td>${instructor.email}</td>
                                <td>${instructor.phoneNumber}</td>
                                <td>
                                    <input type="checkbox" readonly disabled ${instructor.isadjunct ? 'checked' : ''} />
                                </td>
                                <td>
                                    <a href="instructorDetail?instructorId=${instructor.instructorId}" title="Edit Instructor">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="deleteInstructorButton" data-instructor-id="${instructor.instructorId}" style="margin-left:10px;" title="Delete Instructor">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="modal fade" id="delete-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Instructor
                        </h5>
                        <input type="hidden" id="deleteModalInstructorId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">�</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this instructor?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
        </div>
    </jsp:body>
</t:layout>