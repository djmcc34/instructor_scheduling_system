<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="rowNumber" value="0" scope="page" />

<t:layout>
    <jsp:attribute name="pageTitle">
        Instructor Info
    </jsp:attribute>

    <jsp:attribute name="styles">
    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script src="scripts/lib/handlebars/handlebars.js"></script>
        <script src="scripts/lib/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="scripts/lib/jquery-validation/dist/additional-methods.min.js"></script>
        <script src="scripts/instructors/instructorDetail.js"></script>

        <!--handlebars template for qualifications-->
        <!-- this will allow us to dynamically add rows to the qualifications table from the javascript -->
        <script id="qualification-template" type="text/x-handlebars-template">
            <tr  id="" data-row-number="{{rowNumber}}">
                <td>
                    <!-- naming these selects the same will allow us to get an array of values on the server -->
                    <select class="custom-select" id="qualificationCourseIds" name="qualificationCourseIds">
                        <option value="">Select Course</option>
                        {{#each courses}}
                            <option value="{{courseId}}">{{text}}</option>
                        {{/each}}
                    </select>
                </td>
                <td>
                    <a href="javascript:void(0)" class="deleteQualificationButton" data-instructor-id="" style="margin-left:10px;" title="Delete Qualification">
                        <i class="far fa-trash-alt"></i>
                    </a>                                                
                </td>
            </tr>
        </script>
</jsp:attribute>            

<jsp:body>
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Edit Instructor</div>
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Instructor Info
        </h6>
        <div class="card-body">
            <form action="instructorDetail" method="POST" id="instructorForm">
                <input type="hidden" id="instructorId" name="instructorId" value="${instructorId}" />
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label class="form-label">First Name</label>
                        <input id="instructorFirstName" name="instructorFirstName" type="text" class="form-control col-md-6" placeholder="First Name" value="${instructorFirstName}" autocomplete="off">
                    </div>
                    <div class="form-group col-md-12">
                        <label class="form-label">Middle Initial</label>
                        <input id="instructorMiddleInitial" name="instructorMiddleInitial" type="text" class="form-control col-md-6" placeholder="Middle Initial" value="${instructorMiddleInitial}" autocomplete="off">
                    </div> 
                    <div class="form-group col-md-12">
                        <label class="form-label">Last Name</label>
                        <input id="instructorLastName" name="instructorLastName" type="text" class="form-control col-md-6" placeholder="Last Name" value="${instructorLastName}" autocomplete="off">
                    </div>        
                    <div class="form-group col-md-12">
                        <label class="form-label">Email Address</label>
                        <input id="instructorEmail" name="instructorEmail" type="text" class="form-control col-md-6" placeholder="Email Address" value="${instructorEmail}" autocomplete="off">
                    </div> 
                    <div class="form-group col-md-12">
                        <label class="form-label">Phone Number</label>
                        <input id="instructorPhone" name="instructorPhone" type="text" class="form-control col-md-6" placeholder="PhoneNumber" value="${instructorPhone}" autocomplete="off">
                    </div> 
                    <div class="form-group col-md-12">
                        <label class="form-label">Status</label>
                        <select class="custom-select col-md-6" id="instructorStatus" name="instructorStatus">
                            <option value="">Select status</option>
                            <option value="active" ${instructorStatus == 'active' ? 'selected' : ''}>Active</option>
                            <option value="suspended" ${instructorStatus == 'suspended' ? 'selected' : ''}>Suspended</option>
                            <option value="deleted" ${instructorStatus == 'deleted' ? 'selected' : ''}>Deleted</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="custom-control custom-checkbox m-0">
                            <input type="checkbox" class="custom-control-input" id="isAdjunct" name="isAdjuct" ${instructorAdjunct == 'true' ? 'checked' : ''}>
                            <span class="custom-control-label">Is Adjunct Professor</span>
                        </label>
                    </div>                               
                </div>  
                <h4 class="d-flex justify-content-between align-items-center">
                    Courses Qualified to Teach
                </h4>    
                <div class="row">
                    <div class="col-md-6 table-responsive">
                     <table  id="qualifications-list" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Course Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${qualifications}" var="qualification">
                                <tr id="${qualification.id}" data-row-number="${rowNumber}">
                                    <td>${qualification.courseName}</td>
                                        <td>
                                            <a href="javascript:void(0)" class="deleteQualificationButton" data-qualification-id="${qualification.id}" style="margin-left:10px;" title="Delete Qualification">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <c:set var="rowNumber" value="${rowNumber + 1}" scope="page"/>
                            </c:forEach>
                        </tbody>
                    </table>  
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-6 ">
                        <a href="javascript:void(0)"id="spinner" class="float-md-right" style="display:none;" ><i class="fa fa-spinner fa-spin"></i></a>
                        <a href="javascript:void(0)" class="float-md-right" id="addQualificationButton"><i class="fa fa-plus"></i> add qualification</a>
                    </div>
                </div>
                <a href="instructors" class="btn btn-secondary">Cancel</a>
                <input type="submit" class="btn btn-primary" value="Save">
            </form>
        </div>
                            
         <div class="modal fade" id="delete-qualification-modal">
            <div class="modal-dialog">
                <form class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Delete Qualification
                        </h5>
                        <input type="hidden" id="deleteModalQualificationId" value="">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this qualification? The instructor will no longer be able to be scheduled for this course.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirmButton">Yes, delete</button>
                    </div>
                </form>
            </div>
    </jsp:body>
</t:layout>