/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for creating and maintaining Instructors.
 * @author Team Bravo
 */
@Entity
@Table(name = "instructors")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Instructors.findAll", query = "SELECT i FROM Instructor i")
    , @NamedQuery(name = "Instructors.findByInstructorId", query = "SELECT i FROM Instructor i WHERE i.instructorId = :instructorId")
    , @NamedQuery(name = "Instructors.findByFirstName", query = "SELECT i FROM Instructor i WHERE i.firstName = :firstName")
    , @NamedQuery(name = "Instructors.findByMiddleInitial", query = "SELECT i FROM Instructor i WHERE i.middleInitial = :middleInitial")
    , @NamedQuery(name = "Instructors.findByLastName", query = "SELECT i FROM Instructor i WHERE i.lastName = :lastName")
    , @NamedQuery(name = "Instructors.findByInstructorStatus", query = "SELECT i FROM Instructor i WHERE i.instructorStatus = :instructorStatus")
    , @NamedQuery(name = "Instructors.findByEmail", query = "SELECT i FROM Instructor i WHERE i.email = :email")
    , @NamedQuery(name = "Instructors.findByPhoneNumber", query = "SELECT i FROM Instructor i WHERE i.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Instructors.findByIsadjunct", query = "SELECT i FROM Instructor i WHERE i.isadjunct = :isadjunct")})
public class Instructor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Basic(optional = false)
    @NotNull
    @Column(name = "instructor_id")
    private Integer instructorId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "middle_initial")
    private String middleInitial;
    @Size(max = 2147483647)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 2147483647)
    @Column(name = "instructor_status")
    private String instructorStatus;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 2147483647)
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "isadjunct")
    private Boolean isadjunct;

    /**
     * Default Constructor.
     */
    public Instructor() {
    }

    /**
     * Constructor with ID
     * @param instructorId The ID to set for this new Instructor.
     */
    public Instructor(Integer instructorId) {
        this.instructorId = instructorId;
    }

    /**
     * Constructor with specified ID, First name, Middle Initial.
     * @param instructorId ID to set for this new Instructor.
     * @param firstName FirstName to set for this new Instructor.
     * @param middleInitial Middle Initial to set for this new Instructor.
     */
    public Instructor(Integer instructorId, String firstName, String middleInitial) {
        this.instructorId = instructorId;
        this.firstName = firstName;
        this.middleInitial = middleInitial;
    }

    /**
     * Get the ID for this Instructor.
     * @return the ID.
     */
    public Integer getInstructorId() {
        return instructorId;
    }

    /**
     * Set the ID for this Instructor.
     * @param instructorId the ID to set.
     */
    public void setInstructorId(Integer instructorId) {
        this.instructorId = instructorId;
    }

    /**
     * Get the First Name for this Instructor.
     * @return The First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the First Name for this Instructor.
     * @param firstName The First Name to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Get the Middle Inistial for this Instructor.
     * @return The Middle Initial.
     */
    public String getMiddleInitial() {
        return middleInitial;
    }

    /**
     * Set the Middle Initial for this Instructor.
     * @param middleInitial The middle initial to set.
     */
    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    /**
     * Get the Last Name for this Instructor.
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the Last Name for this Instructor.
     * @param lastName The Last Name to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get th eInstructor Status, Active, Suspended, Deleted etc.
     * @return The status.
     */
    public String getInstructorStatus() {
        return instructorStatus;
    }

    /**
     * Set the Instructor Status for this Instructor.
     * @param instructorStatus The Status to be set.
     */
    public void setInstructorStatus(String instructorStatus) {
        this.instructorStatus = instructorStatus;
    }

    /**
     * Get the Instructor email.
     * @return the email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set this Instructor email address.
     * @param email the email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Get this Instructor Phone number.
     * @return the phone number.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Set the Instructor Phone number.
     * @param phoneNumber The number to set.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Get if this Instructor is an adjunct Professor.
     * @return true if adjunct.
     */
    public Boolean getIsadjunct() {
        return isadjunct;
    }

    /**
     * Set if this Instructor is an adjunct Professor.
     * @param isadjunct true if adjunct.
     */
    public void setIsadjunct(Boolean isadjunct) {
        this.isadjunct = isadjunct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instructorId != null ? instructorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instructor)) {
            return false;
        }
        Instructor other = (Instructor) object;
        if ((this.instructorId == null && other.instructorId != null) || (this.instructorId != null && !this.instructorId.equals(other.instructorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Instructors[ instructorId=" + instructorId + " ]";
    }
    
}
