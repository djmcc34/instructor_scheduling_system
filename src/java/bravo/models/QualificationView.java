/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class that provides some better information to about Qualifications.
 * @author Team Bravo
 */
@Entity
@Table(name = "qualifications_view2")
@XmlRootElement
public class QualificationView implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne
    private Course courseId;
    @JoinColumn(name = "instructor_id", referencedColumnName = "instructor_id")
    @ManyToOne(optional = false)
    private Instructor instructorId;
    @Column(name = "name")
    private String instructorName;
    @Column(name = "course_name")
    private String courseName;

    /**
     * Default Constructor.
     */
    public QualificationView() {
    }

    /**
     * Constructor with ID.
     * @param id The ID to set.
     */
    public QualificationView(Integer id) {
        this.id = id;
    }
    
    /**
     * Get the ID for this QualificationView.
     * @return the ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID for this QualificationView.
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the Course associated with this QualificationView.
     * @return the Course.
     */
    public Course getCourseId() {
        return courseId;
    }

    /**
     * Set the Course associated with this QualificationView.
     * @param courseId The Course to set.
     */
    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    /**
     * Get the Instructor associated with this QualificationView.
     * @return the Instructor.
     */
    public Instructor getInstructorId() {
        return instructorId;
    }

    /**
     * Set the Instructor associated with this QualificationView.
     * @param instructorId The Instructor to set.
     */
    public void setInstructorId(Instructor instructorId) {
        this.instructorId = instructorId;
    }

    /**
     * Get the Instructor name associated with this QualificationView.
     * @return the Instructor Name.
     */
    public String getInstructorName() {
        return instructorName;
    }

    /**
     * Set the Instructor Name associated with this QualificationView.
     * @param instructorName The Name to set.
     */
    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    /**
     * Get the Course name associated with this QualificationView.
     * @return The Course name.
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * Set the Course name associated with this QualificationView.
     * @param courseName The Course Name to Set.
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Qualification)) {
            return false;
        }
        QualificationView other = (QualificationView) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Qualifications[ id=" + id + " ]";
    }


    
}
