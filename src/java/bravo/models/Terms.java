/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  
 * Class to create schedule terms.
 * @author Team Bravo
 */
@Entity
@Table(name = "terms")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Terms.findAll", query = "SELECT t FROM Terms t")
    , @NamedQuery(name = "Terms.findBySeason", query = "SELECT t FROM Terms t WHERE t.season = :season")
    , @NamedQuery(name = "Terms.findByYear", query = "SELECT t FROM Terms t WHERE t.year = :year")
    , @NamedQuery(name = "Terms.findById", query = "SELECT t FROM Terms t WHERE t.id = :id")})
public class Terms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "season")
    private String season;
    @Basic(optional = false)
    @NotNull
    @Column(name = "_year")
    private int year;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "schedule_id", referencedColumnName = "id")
    @ManyToOne
    private Schedule scheduleId;

    /**
     *
     * Default Constructor.
     */
    public Terms() {
    }

    /**
     *
     * Constructor with specified ID.
     * @param id
     */
    public Terms(Integer id) {
        this.id = id;
    }

    /**
     * Constructor with specified ID, Season and Year.
     * @param id The ID of this Term.
     * @param season Spring, Summer, Fall, Winter. etc.
     * @param year Year of this term.
     */
    public Terms(Integer id, String season, int year) {
        this.id = id;
        this.season = season;
        this.year = year;
    }

    /**
     * Get the season for this term.
     * @return the season.
     */
    public String getSeason() {
        return season;
    }

    /**
     * Set the season for this term.
     * @param season The season to be set.
     */
    public void setSeason(String season) {
        this.season = season;
    }

    /**
     * Get the year for this term.
     * @return the year.
     */
    public int getYear() {
        return year;
    }

    /**
     * Set the year for this term.
     * @param year the year to set.
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Get the ID for this term.
     * @return the ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID for this term.
     * @param id value to be set.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Schedule that is attached to the term.
     * @return The schedule that is attached to this term.
     */
    public Schedule getScheduleId() {
        return scheduleId;
    }

    /**
     * Set the schedule to this term.
     * @param scheduleId The schedule to be set.
     */
    public void setScheduleId(Schedule scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Terms)) {
            return false;
        }
        Terms other = (Terms) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Terms[ id=" + id + " ]";
    }
    
}
