/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for creating and maintaining Qualifications.
 * @author Team Bravo
 */
@Entity
@Table(name = "qualifications")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Qualifications.findAll", query = "SELECT q FROM Qualification q")
    , @NamedQuery(name = "Qualifications.findById", query = "SELECT q FROM Qualification q WHERE q.id = :id")
    , @NamedQuery(name = "Qualifications.findByInstructor", query = "SELECT q FROM Qualification q WHERE q.instructorId = :instructor_id")})
public class Qualification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)      
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne
    private Course courseId;
    @JoinColumn(name = "instructor_id", referencedColumnName = "instructor_id")
    @ManyToOne(optional = false)
    private Instructor instructorId;

    /**
     * Default Constructor.
     */
    public Qualification() {
    }

    /**
     * Constructor with ID, Creates a new Qualification with the supplied ID.
     * @param id the ID to be set.
     */
    public Qualification(Integer id) {
        this.id = id;
    }

    /**
     * Get the ID of this Qualification.
     * @return the ID.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID for this Qualification.
     * @param id the ID to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the Course associated with this Qualification
     * @return the Course associated.
     */
    public Course getCourseId() {
        return courseId;
    }

    /**
     * Set the Course to be associated with this Qualification.
     * @param courseId The Course to associate.
     */
    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    /**
     * Get the Instructor associated with this Qualification.
     * @return The Instructor.
     */
    public Instructor getInstructorId() {
        return instructorId;
    }

    /**
     * Set the Instructor associated with this Qualification.
     * @param instructorId the Instructor to set.
     */
    public void setInstructorId(Instructor instructorId) {
        this.instructorId = instructorId;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Qualification)) {
            return false;
        }
        Qualification other = (Qualification) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Qualifications[ id=" + id + " ]";
    }
    
}
