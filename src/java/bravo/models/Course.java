/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import com.opencsv.bean.CsvBindByName;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for creating and maintaining Courses.
 * @author Team Bravo.
 */
@Entity
@Table(name = "course")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Course.findAll", query = "SELECT c FROM Course c")
    , @NamedQuery(name = "Course.findByDescription", query = "SELECT c FROM Course c WHERE c.description = :description")
    , @NamedQuery(name = "Course.findByShortTitle", query = "SELECT c FROM Course c WHERE c.shortTitle = :shortTitle")
    , @NamedQuery(name = "Course.findById", query = "SELECT c FROM Course c WHERE c.id = :id")
    , @NamedQuery(name = "Course.findByCreditHours", query = "SELECT c FROM Course c WHERE c.creditHours = :creditHours")
    , @NamedQuery(name = "Course.findByCourseNumber", query = "SELECT c FROM Course c WHERE c.courseNumber = :courseNumber")})
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "description")
    @CsvBindByName(column = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "short_title")
    @CsvBindByName(column = "SHORT_TITLE", required = true)
    private String shortTitle;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @CsvBindByName(column = "CREDIT_HOURS", required = true)
    @Column(name = "credit_hours")
    private Integer creditHours;
    @Column(name = "course_number")
    @CsvBindByName(column = "COURSE_NO", required = true)
    private Integer courseNumber;
    @Column(name = "department_id")
    private Integer departmentId;
    private transient String departmentName;
    @CsvBindByName(column = "SUBJECT", required = true)
    private transient String departmentShortName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;
    
    /**
     * Default Constructor for new Courses.
     */
    public Course() {
    }

    /**
     * Constructor for new Course with specified ID.
     * @param id the ID for this Course.
     */
    public Course(Integer id) {
        this.id = id;
    }

    /**
     * Constructor for new Course with specified ID, Short title.
     * @param id the ID for this Course.
     * @param shortTitle the Short Title for this Course.
     */
    public Course(Integer id, String shortTitle) {
        this.id = id;
        this.shortTitle = shortTitle;
    }

    /**
     * Get the Description for this Course.
     * @return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description for this Course.
     * @param description the description to be set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the soft delete flag for this course.
     * @return true if deleted.
     */
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * Set the soft delete flag for this Course.
     * @param isDeleted true if deleted.
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * Get the short title for this Course.
     * @return the short title.
     */
    public String getShortTitle() {
        return shortTitle;
    }

    /**
     * Set the short title for this Course.
     * @param shortTitle the short title to set.
     */
    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    /**
     * Get the ID for this Course.
     * @return the ID of this Course.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID for this Course.
     * @param id the ID to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get the number of credit hours for this Course.
     * @return the number of hours.
     */
    public Integer getCreditHours() {
        return creditHours;
    }

    /**
     * Set the number of credit hours for this Course.
     * @param creditHours the hours to set.
     */
    public void setCreditHours(Integer creditHours) {
        this.creditHours = creditHours;
    }

    /**
     * Get the Course Number for this Course.
     * @return the Course Number.
     */
    public Integer getCourseNumber() {
        return courseNumber;
    }

    /**
     * Set the Course number for this Course.
     * @param courseNumber the number to set.
     */
    public void setCourseNumber(Integer courseNumber) {
        this.courseNumber = courseNumber;
    }

    /**
     * Get the Department ID associated with this Course.
     * @return the ID for the Department.
     */
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**
     * Set the ID for the Department this Course is under.
     * @param departmentId the ID to set.
     */
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
    
    /**
     * Get the Name of the Department this Course is associated with.
     * @return The ID of the Department associated with this Course.
     */
    public Integer getName() {
        return departmentId;
    }

    /**
     * Get the Department Name associated with the Department this Course is in.
     * @return the Department Name.
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Set the Department Name this Course is associated with.
     * @param departmentName The Department Name to set.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Get the Department Short name this course is associated with.
     * @return the Department short name.
     */
    public String getDepartmentShortName() {
        return departmentShortName;
    }

    /**
     * Set the Department short name this Course is associated with.
     * @param departmentShortName Department short name to set.
     */
    public void setDepartmentShortName(String departmentShortName) {
        this.departmentShortName = departmentShortName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Course[ id=" + id + " ]";
    }

}
