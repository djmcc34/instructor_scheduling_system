/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class for creating and maintaining Departments.
 * @author Team Bravo.
 */
@Entity
@Table(name = "department")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Department.findAll", query = "SELECT d FROM Department d")
    , @NamedQuery(name = "Department.findByDepartmentId", query = "SELECT d FROM Department d WHERE d.departmentId = :departmentId")
    , @NamedQuery(name = "Department.findByDepartmentName", query = "SELECT d FROM Department d WHERE d.departmentName = :departmentName")
    , @NamedQuery(name = "Department.findByIsDeleted", query = "SELECT d FROM Department d WHERE d.isDeleted = :isDeleted")})
public class Department implements Serializable {

    @Size(max = 2147483647)
    @Column(name = "short_name")
    private String shortName;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "department_id")
    private Integer departmentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "department_name")
    private String departmentName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;

    /**
     * Default Constructor.
     */
    public Department() {
    }

    /**
     * Constructor with specified ID.
     * @param departmentId The ID to set.
     */
    public Department(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * Constructor with specified ID, Name and soft delete flag.
     * @param departmentId The ID to set.
     * @param departmentName The name of this Department.
     * @param isDeleted Flag for soft delete, true if deleted.
     */
    public Department(Integer departmentId, String departmentName, boolean isDeleted) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.isDeleted = isDeleted;
    }

    /**
     * Get the ID for this Department.
     * @return the ID.
     */
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**
     * Set the ID for this Department.
     * @param departmentId the ID to set.
     */
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * Get the name of this Department.
     * @return this Department name.
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Set the name for this Department.
     * @param departmentName the name to set.
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Get the soft delete flag for this Department.
     * @return true if soft deleted.
     */
    public boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * Set the soft delete flag for this Department.
     * @param isDeleted the value to set, true if deleted.
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * Get the Short name for this Department, Comp, Mis, Math etc.
     * @return the Short name for this Department.
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Set the Short name for this Department, Comp, Mis, Math etc. 
     * @param shortName the name to set.
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departmentId != null ? departmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Department)) {
            return false;
        }
        Department other = (Department) object;
        if ((this.departmentId == null && other.departmentId != null) || (this.departmentId != null && !this.departmentId.equals(other.departmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Department[ departmentId=" + departmentId + " ]";
    }
}
