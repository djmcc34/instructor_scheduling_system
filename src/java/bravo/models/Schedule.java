/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for creating and maintaining Schedules.
 * @author Team Bravo
 */
@Entity
@Table(name = "schedule")
@XmlRootElement
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "schedule_id")
    private Integer scheduleId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "term")
    private String term;

    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "section")
    private String section;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "location")
    private String location;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @Size(max = 2147483647)
    @Column(name = "course_days")
    private String courseDays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Course course;
    @JoinColumn(name = "instructor_id", referencedColumnName = "instructor_id")
    @ManyToOne(optional = false)
    private Instructor instructor;

    /**
     * Default Constructor.
     */
    public Schedule() {
    }

    /**
     * Constructor with specific ID.
     * @param scheduleId ID for this Schedule.
     */
    public Schedule(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * Constructor that specifies, ID, Term, Section, Location, Start and End Dates, Status, and IsDeleted.
     * @param scheduleId ID for this schedule
     * @param term Term for this schedule Spring, Fall, Winter, Summer.
     * @param section Section for this schedule ex. Q1WW, R1WW etc.
     * @param location Location for this Schedule Downtown, Online etc.
     * @param startDate Date this Schedule starts on.
     * @param endDate Date this schedule ends on.
     * @param status Instructor status of this schedule, Pending, Approved, Denied.
     * @param isDeleted true/false soft delete flag.
     */
    public Schedule(Integer scheduleId, String term, String section, String location, Date startDate, Date endDate, String status, boolean isDeleted) {
        this.scheduleId = scheduleId;
        this.term = term;
        this.section = section;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.isDeleted = isDeleted;
    }

    /**
     * Get the ID of this Schedule.
     * @return ID of this Schedule.
     */
    public Integer getScheduleId() {
        return scheduleId;
    }

    /**
     * Set the ID of this schedule.
     * @param scheduleId The ID to be set.
     */
    public void setScheduleId(Integer scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * Get the term for this Schedule.
     * @return the term.
     */
    public String getTerm() {
        return term;
    }

    /**
     * Set the term for this Schedule.
     * @param term the term to set.
     */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
     * Get the year for this Schedule.
     * @return The year.
     */
    public int getYear() {
        return year;
    }

    /**
     * Set the year for this Schedule.
     * @param year The year to set.
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Get the Section for this Schedule.
     * @return The section for this Schedule.
     */
    public String getSection() {
        return section;
    }

    /**
     * Set the Section for this Schedule.
     * @param section The Section to set.
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * Get the Location for this Schedule.
     * @return The location.
     */
    public String getLocation() {
        return location;
    }

    /** 
     * Set the Location for this Schedule.
     * @param location The Location to set.
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get the start date for this Schedule.
     * @return The start date.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Set the Start Date for this Schedule.
     * @param startDate The date to set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Get the End Date for this Schedule.
     * @return the date.
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Set the End Date for this Schedule.
     * @param endDate the date to set.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Get the Start Time for this Schedule.
     * @return The Time.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Set the Start Time for this Schedule.
     * @param startTime The time to set.
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Get the End Time for this Schedule.
     * @return the time.
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Set the End Time for this Schedule.
     * @param endTime time to set.
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Get the Days of the week for this Schedule.
     * @return the days.
     */
    public String getCourseDays() {
        return courseDays;
    }

    /**
     * Set the days of the week this course is held on.
     * @param courseDays The days the course will meet each week.
     */
    public void setCourseDays(String courseDays) {
        this.courseDays = courseDays;
    }

    /**
     * The Status of the instructor of this course, Pending, Approved, Denied.
     * @return The instructor status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the instructor status for this Schedule.
     * @param status that status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Get the soft delete flag, true if deleted.
     * @return soft delete flag, true if deleted.
     */
    public boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * Set the soft delete flag for this Schedule.
     * @param isDeleted true/false if this Schedule is soft deleted.
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * Get the course associated with this Schedule.
     * @return the course.
     */
    public Course getCourse() {
        return course;
    }

    /**
     * Set the course associated with this Schedule.
     * @param course the course to set.
     */
    public void setCourse(Course course) {
        this.course = course;
    }

    /**
     * Get the Instructor associated with the course for this Schedule.
     * @return the Instructor.
     */
    public Instructor getInstructor() {
        return instructor;
    }

    /**
     * Set the Instructor associated with the course for this Schedule.
     * @param instructor The Instructor to set.
     */
    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scheduleId != null ? scheduleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Schedule)) {
            return false;
        }
        Schedule other = (Schedule) object;
        if ((this.scheduleId == null && other.scheduleId != null) || (this.scheduleId != null && !this.scheduleId.equals(other.scheduleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bravo.models.Schedule[ scheduleId=" + scheduleId + " ]";
    }
}
