/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for creating and maintaining Logins.
 * @author Team Bravo
 */
@Entity
@Table(name = "login")
@XmlRootElement
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "login_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer loginId;
    @NotNull
    @Column(name = "emailAddress")
    private String emailAddress;
    @NotNull
    @Column(name = "password")
    private String password;

    /**
     * Get the ID for this Login.
     * @return the ID.
     */
    public Integer getLoginId() {
        return loginId;
    }

    /**
     * Set the Login ID for this this Login.
     * @param id The ID to set.
     */
    public void setLoginId(Integer id) {
        this.loginId = id;
    }

    /**
     * Default Constructor.
     */
    public Login() {
    }

    /**
     * Get the email address for this Login.
     * @return The email address.
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Set the email address for this Login.
     * @param emailAddress to be set.
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Get the password for this Login.
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password for this Login.
     * @param password The password to be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginId != null ? loginId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Login)) {
            return false;
        }
        Login other = (Login) object;
        return !((this.loginId == null && other.loginId != null) || (this.loginId != null && !this.loginId.equals(other.loginId)));
    }

    @Override
    public String toString() {
        return "bravo.models.Login[ id=" + loginId + " ]";
    }

}
