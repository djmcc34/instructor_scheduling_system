/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.utils;

/**
 * Class used to specify alert message types.
 * @author Team Bravo
 */
public enum AlertMessageType {

    /**
     * Alert Message type on Success.
     */
    Success,

    /**
     * Alert Message type on Info.
     */
    Info,

    /**
     * Alert Message type on Warnings.
     */
    Warning,

    /**
     * Alert Message type on Errors.
     */
    Error
}
