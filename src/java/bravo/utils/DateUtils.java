/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class used to provide date/time formatting.
 * @author Team Bravo
 */
public class DateUtils {

    /**
     * Method to format date strings into MM/DD/YYYY.
     * @param date The date to be formatted.
     * @return the formatted date.
     */
    public static String formatDate(Date date) {
        if (date != null) {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            return format.format(date);
        }

        return null;
    }

    /**
     * Method to format time into AM/PM format.
     * @param time The time to format.
     * @return the formatted time.
     */
    public static String formatTime(Date time) {
        if (time != null) {
            DateFormat format = new SimpleDateFormat("h:mm a", Locale.ENGLISH);
            return format.format(time);
        }

        return null;
    }

    /**
     * Method to format time into a 24hr format.
     * @param time The time to format.
     * @return the formatted time.
     */
    public static String format24HourTime(Date time) {
        if (time != null) {
            DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            return format.format(time);
        }

        return null;
    }
}
