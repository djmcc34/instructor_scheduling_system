/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.utils;

import bravo.models.Schedule;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Class used to send email correspondence from the application.
 * @author Team Bravo
 */


public class MailUtil {
    
    /**
     * Method used to send email confirmation requests to Instructors who have
     * been scheduled as a course instructor and their status is pending.
     * @param schedule The Schedule and Course for the Instructor to confirm.
     * @throws MessagingException
     */
    public static void sendConfirmationEmail(Schedule schedule) throws MessagingException {
        
        if(schedule.getInstructor() != null &&
                schedule.getStatus().equalsIgnoreCase("pending")) {
            
        
        
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.quitwait", "false");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            
            Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("franklin.f18bravo@gmail.com",
                                                "javabeans#");
				}
			});
            session.setDebug(true);
            
            Message message = new MimeMessage(session);
            
            
            SimpleDateFormat sdf_date = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat sdf_time = new SimpleDateFormat("hh:mm a");
            
            String subject = "(Action Required) You have been selected as an Instructor at Franklin University";
            String mailText = "Hello " + schedule.getInstructor().getFirstName()
                    + " " + schedule.getInstructor().getLastName() + ",\n\n"
                    + "You have been selected as an instructor at Franklin University "
                    + "to teach the course number " 
                    + schedule.getCourse().getCourseNumber() + "-" + schedule.getSection() + " "
                    + schedule.getCourse().getShortTitle() + " \n\n"
                    + "The schedule of this course is incomplete please follow the link below "
                    + "to accept or decline this teaching opportunity at Franklin University. \n\n"
                    + "http://instructordb.us-east-2.elasticbeanstalk.com/confirmation?scheduleId="
                    + schedule.getScheduleId()
                    + "&" + "instructorId=" + schedule.getInstructor().getInstructorId() + "\n\n"
                    + "****************************\n"
                    + "* Course information: \n"
                    + "****************************\n"
                    + "Course: " + schedule.getCourse().getCourseNumber() 
                    + schedule.getSection() + " "
                    + schedule.getCourse().getShortTitle() + "\n"
                    + "Start: " + sdf_date.format(schedule.getStartDate()) + "\n"
                    + "Ends: " + sdf_date.format(schedule.getEndDate()) + "\n"
                    + "****************************\n\n"
                    + "Communication sent on " + new Date();
                    
            
            message.setSubject(subject);
            message.setText(mailText);
            
            Address fromAddress = new InternetAddress("franklin.f18bravo@gmail.com");
            
            
            message.setFrom(fromAddress);
            message.setRecipients(Message.RecipientType.TO, 
                    InternetAddress.parse("franklin.f18bravo+" + schedule.getInstructor().getLastName() + "@gmail.com"));
            
            Transport.send(message);
            
        
        } 
    }
    
}