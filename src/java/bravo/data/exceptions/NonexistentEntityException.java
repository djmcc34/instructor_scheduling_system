package bravo.data.exceptions;

/**
 * Class to Create and return NonexistentEntityExceptions.
 * @author Team Bravo.
 */
public class NonexistentEntityException extends Exception {

    /**
     * Constructor with a message and error cause.
     * @param message The error message.
     * @param cause The throwable error that caused it.
     */
    public NonexistentEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Method to set the message for this type of error.
     * @param message The message to be set.
     */
    public NonexistentEntityException(String message) {
        super(message);
    }
}
