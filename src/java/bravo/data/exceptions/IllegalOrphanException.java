package bravo.data.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 *Class for Creating and Listing Illegal Orphan Exceptions.
 * @author Team Bravo.
 */
public class IllegalOrphanException extends Exception {
    private List<String> messages;

    /**
     * Constructor with a list of messages.
     * @param messages the list of messages.
     */
    public IllegalOrphanException(List<String> messages) {
        super((messages != null && messages.size() > 0 ? messages.get(0) : null));
        if (messages == null) {
            this.messages = new ArrayList<String>();
        }
        else {
            this.messages = messages;
        }
    }

    /**
     * Method to return the list of messages.
     * @return the list.
     */
    public List<String> getMessages() {
        return messages;
    }
}
