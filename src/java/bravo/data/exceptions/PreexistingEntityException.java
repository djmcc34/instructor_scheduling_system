package bravo.data.exceptions;

/**
 * Class to control Preexisting Entity exceptions.
 * @author Team Bravo.
 */
public class PreexistingEntityException extends Exception {

    /**
     * Constructor with a message and the cause of the error.
     * @param message The message to set.
     * @param cause The cause of the error.
     */
    public PreexistingEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Method to set the Message for this error.
     * @param message the message to be set.
     */
    public PreexistingEntityException(String message) {
        super(message);
    }
}
