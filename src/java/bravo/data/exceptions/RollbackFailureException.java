package bravo.data.exceptions;

/**
 * Class to handle Rollback Failure exceptions.
 * @author Team Bravo.
 */
public class RollbackFailureException extends Exception {

    /**
     * Constructor for this error, takes a message and a cause.
     * @param message The error message.
     * @param cause What caused the error.
     */
    public RollbackFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Method to set the Message for this exception.
     * @param message the message to set.
     */
    public RollbackFailureException(String message) {
        super(message);
    }
}
