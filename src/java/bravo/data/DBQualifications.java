/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.data.exceptions.NonexistentEntityException;
import bravo.data.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import bravo.models.Course;
import bravo.models.Instructor;
import bravo.models.QualificationView;
import bravo.models.Qualification;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Class that handles Qualification interactions within the database.
 * @author Team Bravo.
 */
public class DBQualifications implements Serializable {

    private EntityManagerFactory emf = null;

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf The Entity Manager Factory.
     */
    public DBQualifications(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * Method to return the Entity Manager for use in this object.
     * @return the Entity Manager.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to Create a new Qualification in the database.
     * @param qualification The Qualification to create.
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(Qualification qualification) throws PreexistingEntityException, Exception {
        List<Qualification> quals = new ArrayList<>();
        create(quals);
    }

    /**
     * Method used to create multiple qualifications at once.
     * @param qualifications The List of Qualifications to create.
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(List<Qualification> qualifications) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            
            for(Qualification qual: qualifications) {
                em.persist(qual);
            }
           
            em.getTransaction().commit();
        } catch (Exception ex) {
            throw new Exception("Error occured creating record", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to remove a specified Qualification from the database, there is no soft delete.
     * @param id The ID of the Qualification to remove.
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Qualification q;
            q = em.getReference(Qualification.class, id);
            em.remove(q);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("Error occured deleting records", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find a Qualification based on the ID within the database.
     * @param id The ID of the Qualification to find.
     * @return
     */
    public Qualification findQualifications(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Qualification.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Find Qualifications based on a supplied Instructor.
     * @param instructor The Instructor we wish to find Qualifications for.
     * @return The List of the supplied Instructor Qualifications.
     * @throws Exception
     */
    public List<QualificationView> findQualificationsEntitiesByInstructor(Instructor instructor) throws Exception {
        EntityManager em = getEntityManager();
        try {
            //   System.out.println("Instructor ID passed to query is: " + instructor.getInstructorId());
            //List<Qualifications> q = em.createNamedQuery("Qualifications.findByInstructor")
            //        .setParameter("instructor_id", instructor).getResultList();

            List<QualificationView> q = em.createNativeQuery("SELECT * FROM qualifications_view2 WHERE instructor_id = ?", QualificationView.class)
                    .setParameter(1, instructor.getInstructorId()).getResultList();

            return q;
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("Error occured finding records", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    /**
     * Find the list of Qualified Instructors based on a Course.
     * @param course The Course we are looking for Qualified Instructors for.
     * @return The list of Instructors Qualified to teach the supplied Course.
     * @throws Exception
     */
    public List<QualificationView> findQualificationsEntitiesByCourse(Course course) throws Exception {
        EntityManager em = getEntityManager();
        try {
            //   System.out.println("Instructor ID passed to query is: " + instructor.getInstructorId());
            //List<Qualifications> q = em.createNamedQuery("Qualifications.findByInstructor")
            //        .setParameter("instructor_id", instructor).getResultList();
            
            List<QualificationView> q = em.createNativeQuery("SELECT * FROM qualifications_view2 WHERE course_id = ?", QualificationView.class)
                    .setParameter(1, course.getId()).getResultList();
            
            return q;
        } catch (Exception ex) {
            System.out.println(ex);
            throw new Exception("Error occured finding records", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}