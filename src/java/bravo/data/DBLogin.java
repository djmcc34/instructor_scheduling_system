/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.models.Login;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for handling Login to the Database.
 * @author Team Bravo.
 */
public class DBLogin implements Serializable {
    
    private final String QUERY = "SELECT * FROM login WHERE upper(email_address) = upper(?)";

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf
     */
    public DBLogin(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    /**
     * Method to return the Entity Manager for this object.
     * @return The Entity Manager.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to find an email address in the database for login.
     * @param emailAddress The email to Find.
     * @return The List of logins.
     * @throws Exception
     */
    public List<Login> findLogin(String emailAddress) throws Exception {
        EntityManager em = getEntityManager();
        try {

            List<Login> login = em.createNativeQuery(QUERY, Login.class)
                    .setParameter(1, emailAddress).getResultList();
            return login;
        } catch (Exception ex) {
            throw new Exception("Error occured finding records", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
