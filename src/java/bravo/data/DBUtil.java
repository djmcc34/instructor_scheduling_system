/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class to manage getting an Entity Manager Factory for use in this Application.
 * @author Team Bravo
 */
public class DBUtil {

    /**
     * Method to return the Entity Manager Factory for this Application.
     * @return the Entity Manager Factory.
     */
    public static EntityManagerFactory getEmFactory() {
        return Persistence.createEntityManagerFactory("Instructor_Scheduling_SystemPU");
    }
}
