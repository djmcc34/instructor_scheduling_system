/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.data.exceptions.IllegalOrphanException;
import bravo.data.exceptions.NonexistentEntityException;
import bravo.data.exceptions.PreexistingEntityException;
import bravo.models.Course;
import bravo.models.Instructor;
import bravo.models.Instructor_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Class to handle database interactions involving Instructors.
 * @author Team Bravo.
 */
public class DBInstructors implements Serializable {

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf The Entity Manager Factory to use.
     */
    public DBInstructors(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    /**
     * Method to return an Entity Manager for this object.
     * @return the Entity Manager.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to create a new Instructor in the database.
     * @param instructors The Instructor to create.
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(Instructor instructors) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instructors);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to edit an existing Instructor in the database.
     * @param instructors The Instructor to update.
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Instructor instructors) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.merge(instructors);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to get a list of Instructors excluding those that have not been soft deleted.
     * @return The list of Instructors.
     */
    public List<Instructor> findInstructorsEntities() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Instructor> inst = cq.from(Instructor.class);

            cq.select(inst);

            //filter out instructors with a status of deleted
            cq.where(builder.and(
                    builder.notEqual(inst.get(Instructor_.instructorStatus), "deleted")
            ));

            Query q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Method to find Instructors that are Qualified to instruct a Course.
     * @param course The Course the Instructors should be qualified to teach.
     * @return The list of qualified Instructors.
     */
    public List<Instructor> findByQualified(Course course) {
        EntityManager em = getEntityManager();
        try {
            List<Instructor> instructors = em.createNativeQuery(
                    "select instructors.* "
                    + "from instructors "
                    + "inner join qualifications on instructors.instructor_id = qualifications.instructor_id "
                    + "inner join course on qualifications.course_id = course.id "
                    + "where instructors.instructor_status != 'deleted' "
                    + "and course.is_deleted = false "
                    + "and course.id = ?", Instructor.class)
                    .setParameter(1, course.getId())
                    .getResultList();

            return instructors;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find Instructor in the database matching a specific ID.
     * @param id The ID of the Instructor to find.
     * @return The Instructor.
     */
    public Instructor findInstructors(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Instructor.class, id);
        } finally {
            em.close();
        }
    }
}
