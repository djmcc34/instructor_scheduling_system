/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import bravo.models.Department;
import bravo.models.Department_;
import bravo.models.QualificationView;
import bravo.models.Schedule;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Class to handle database manipulation involving Departments.
 * @author Team Bravo.
 */
public class DBDepartment implements Serializable {

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf The Entity Manager Factory to use.
     */
    public DBDepartment(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    /**
     * Method to get the Entity Manager.
     * @return the Entity Manager.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to find all Departments that have not been soft deleted.
     * @return The list of Departments.
     */
    public List<Department> findDepartmentEntities() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Department> dept = cq.from(Department.class);

            cq.select(dept);

            //filter out departments where isDeleted = true
            cq.where(builder.and(
                    builder.equal(dept.get(Department_.isDeleted), false)
            ));

            Query q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Method to find a specific Department based on its ID.
     * @param id The ID of the Department to find.
     * @return the Department.
     */
    public Department findDepartment(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Department.class, id);
        } finally {
            em.close();
        }
    }

    /**
     * Method to find a Department based on its Short Name.
     * @param shortName The Short Name of the Department to be found.
     * @return The Department.
     * @throws Exception
     */
    public Department findDepartmentByShortName(String shortName) throws Exception {
        EntityManager em = getEntityManager();
        try {

            List<Department> q = em.createNativeQuery("SELECT * FROM department WHERE short_name = UPPER(?)", Department.class)
                    .setParameter(1, shortName).getResultList();
            return q.get(0);
        } catch (Exception ex) {
            throw new Exception("Error occured finding records", ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to Create a new Department in the database.
     * @param department The Department to be created.
     */
    public void create(Department department) {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(department);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to edit an existing Department in the database.
     * @param department the Department that needs to be edited.
     * @throws Exception
     */
    public void edit(Department department) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.merge(department);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /** 
     * Method to handle soft deleting a Department from the database.
     * @param departmentid The Department ID to be soft deleted.
     * @throws Exception
     */
    public void delete(int departmentid) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Department department = findDepartment(departmentid);
            department.setIsDeleted(true);

            em.merge(department);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
