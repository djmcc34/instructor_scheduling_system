/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.data.exceptions.NonexistentEntityException;
import bravo.data.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import bravo.models.Course;
import bravo.models.Department;
import bravo.models.Instructor;
import bravo.models.Schedule;
import bravo.models.Schedule_;
import bravo.models.Terms;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 * Class used to control Schedule interaction with the database.
 * @author Team Bravo.
 */
public class DBSchedule implements Serializable {

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf
     */
    public DBSchedule(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    /**
     * Method to get the Entity Manager used in this object.
     * @return the Entity Manager to use.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to create a new Schedule in the database.
     * @param schedule The Schedule to create in the database.
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(Schedule schedule) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.persist(schedule);

            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSchedule(schedule.getScheduleId()) != null) {
                throw new PreexistingEntityException("Schedule " + schedule + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to edit an existing Schedule in the database.
     * @param schedule The Schedule that needs to be edited.
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Schedule schedule) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            schedule = em.merge(schedule);

            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = schedule.getScheduleId();
                if (findSchedule(id) == null) {
                    throw new NonexistentEntityException("The schedule with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method used to soft delete a Schedule from the database.
     * @param scheduleId The ID of the Schedule to be soft deleted.
     * @throws Exception
     */
    public void delete(int scheduleId) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Schedule schedule = findSchedule(scheduleId);
            schedule.setIsDeleted(true);

            em.merge(schedule);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to return a list of Schedules based on supplied Department Term and Year information.
     * @param department The Department that the Schedules are associated with.
     * @param term The Term the Schedule is associated with.
     * @param year The Year the Schedule is associated with.
     * @return
     */
    public List<Schedule> findByDepartmentAndTerm(Department department, String term, int year) {
        EntityManager em = getEntityManager();
        try {
            List<Schedule> schedule = em.createNativeQuery(
                    "SELECT schedule.* FROM schedule "
                    + "INNER JOIN course on schedule.course_id = course.id "
                    + "WHERE course.department_id = ? "
                    + "AND schedule.term = ? "
                    + "AND schedule.year = ? "
                    + "AND schedule.is_deleted = false", Schedule.class)
                    .setParameter(1, department.getDepartmentId())
                    .setParameter(2, term)
                    .setParameter(3, year)
                    .getResultList();

            return schedule;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find a list of Schedules based on a supplied Department.
     * @param department The Department for which a Schedule List should be returned.
     * @return The list of Schedules.
     */
    public List<Schedule> findByDepartment(Department department) {
        EntityManager em = getEntityManager();
        try {
            List<Schedule> schedule = em.createNativeQuery(
                    "SELECT schedule.* FROM schedule "
                    + "INNER JOIN course on schedule.course_id = course.id "
                    + "WHERE course.department_id = ? "
                    + "AND schedule.is_deleted = false", Schedule.class)
                    .setParameter(1, department.getDepartmentId())
                    .getResultList();

            return schedule;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find a list of Schedules based on a supplied Term and Year such as Spring 2019.
     * @param term The Term we are looking for Schedules, ex. Spring, Summer, Winter, Fall.
     * @param year The Year of the Schedule.
     * @return the List of matching Schedules.
     */
    public List<Schedule> findByTerm(String term, int year) {
        EntityManager em = getEntityManager();
        try {
            List<Schedule> schedule = em.createNativeQuery(
                    "SELECT schedule.* FROM schedule "
                    + "WHERE schedule.term = ? "
                    + "AND schedule.year = ? "
                    + "AND schedule.is_deleted = false", Schedule.class)
                    .setParameter(1, term)
                    .setParameter(2, year)
                    .getResultList();

            return schedule;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to return a list of all Schedules that have not been soft deleted.
     * @return the list of Schedules.
     */
    public List<Schedule> findScheduleEntities() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Schedule> schedule = cq.from(Schedule.class);

            cq.select(schedule);

            cq.where(builder.and(
                    builder.equal(schedule.get(Schedule_.isDeleted), false)
            ));

            Query q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Method to return a specified Schedule based on it's ID.
     * @param id The ID of the Schedule to be returned.
     * @return The Schedule.
     */
    public Schedule findSchedule(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Schedule.class, id);
        } finally {
            em.close();
        }
    }
}
