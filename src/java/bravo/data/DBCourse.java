/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.data;

import bravo.models.Course;
import bravo.models.Course_;
import bravo.models.Department;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * Class to handle interaction with the database regarding Course information.
 * @author Team Bravo.
 */
public class DBCourse implements Serializable {

    /**
     * Constructor for this object takes an Entity Manager Factory.
     * @param emf the Entity Manager Factory to use.
     */
    public DBCourse(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    /**
     * Method to return the Entity Manager.
     * @return the Entity Manager.
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Method to Create a new Course in the database.
     * @param course the Course to be created.
     * @throws Exception
     */
    public void create(Course course) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.persist(course);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to edit an existing Course in the database.
     * @param course the Course to be edited.
     * @throws Exception
     */
    public void edit(Course course) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.merge(course);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find all Courses in the database.
     * @return The list of all Courses that are not deleted.
     */
    public List<Course> findCourseEntities() {
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Course> course = cq.from(Course.class);
            cq.select(course);
            cq.where(builder.and(
                    builder.equal(course.get(Course_.isDeleted), false)
            ));
            Query q = em.createQuery(cq);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Method to find Courses in the database by a Department.
     * @param department The Department for which associated courses should be returned.
     * @return The list of Courses.
     */
    public List<Course> findByDepartment(Department department) {
        EntityManager em = getEntityManager();
        try {
            List<Course> courses = em.createNativeQuery(
                    "SELECT course.* FROM course "
                    + "WHERE course.department_id = ? "
                    + "AND course.is_deleted = false", Course.class)
                    .setParameter(1, department.getDepartmentId())
                    .getResultList();

            return courses;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Method to find a Course based on it's CourseID.
     * @param id The Course ID to find.
     * @return The Course.
     */
    public Course findCourse(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Course.class, id);
        } finally {
            em.close();
        }
    }

    /**
     * Method to delete the specified Course.
     * @param courseId The Course ID to remove.
     * @throws Exception
     */
    public void delete(int courseId) throws Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Course course = findCourse(courseId);
            course.setIsDeleted(true);

            em.merge(course);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
