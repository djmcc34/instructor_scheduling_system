/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
package bravo.data;

import bravo.data.exceptions.NonexistentEntityException;
import bravo.data.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import bravo.models.Schedule;
import bravo.models.Terms;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * This Class was Created to Manage Term Interaction with the database.
 * This Class was not implemented.
 * @author Team Bravo
 *
public class DBTerms implements Serializable {

    public DBTerms(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Terms terms) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Schedule scheduleId = terms.getScheduleId();
            if (scheduleId != null) {
                scheduleId = em.getReference(scheduleId.getClass(), scheduleId.getId());
                terms.setScheduleId(scheduleId);
            }
            em.persist(terms);
            if (scheduleId != null) {
                scheduleId.getTermsCollection().add(terms);
                scheduleId = em.merge(scheduleId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTerms(terms.getId()) != null) {
                throw new PreexistingEntityException("Terms " + terms + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Terms terms) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Terms persistentTerms = em.find(Terms.class, terms.getId());
            Schedule scheduleIdOld = persistentTerms.getScheduleId();
            Schedule scheduleIdNew = terms.getScheduleId();
            if (scheduleIdNew != null) {
                scheduleIdNew = em.getReference(scheduleIdNew.getClass(), scheduleIdNew.getId());
                terms.setScheduleId(scheduleIdNew);
            }
            terms = em.merge(terms);
            if (scheduleIdOld != null && !scheduleIdOld.equals(scheduleIdNew)) {
                scheduleIdOld.getTermsCollection().remove(terms);
                scheduleIdOld = em.merge(scheduleIdOld);
            }
            if (scheduleIdNew != null && !scheduleIdNew.equals(scheduleIdOld)) {
                scheduleIdNew.getTermsCollection().add(terms);
                scheduleIdNew = em.merge(scheduleIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = terms.getId();
                if (findTerms(id) == null) {
                    throw new NonexistentEntityException("The terms with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Terms terms;
            try {
                terms = em.getReference(Terms.class, id);
                terms.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The terms with id " + id + " no longer exists.", enfe);
            }
            Schedule scheduleId = terms.getScheduleId();
            if (scheduleId != null) {
                scheduleId.getTermsCollection().remove(terms);
                scheduleId = em.merge(scheduleId);
            }
            em.remove(terms);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Terms> findTermsEntities() {
        return findTermsEntities(true, -1, -1);
    }

    public List<Terms> findTermsEntities(int maxResults, int firstResult) {
        return findTermsEntities(false, maxResults, firstResult);
    }

    private List<Terms> findTermsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Terms.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Terms findTerms(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Terms.class, id);
        } finally {
            em.close();
        }
    }

    public int getTermsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Terms> rt = cq.from(Terms.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
*/