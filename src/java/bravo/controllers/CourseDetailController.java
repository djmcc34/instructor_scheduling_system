/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBInstructors;
import bravo.data.DBQualifications;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static java.lang.Integer.parseInt;
import javax.persistence.EntityManagerFactory;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Department;
import bravo.models.Instructor;
import bravo.models.Qualification;
import bravo.models.QualificationView;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Controller that manages actions in the /courseDetail page.
 * @author Team Bravo
 */
@WebServlet(name = "CourseDetailController", urlPatterns = {"/courseDetail"})
public class CourseDetailController extends BaseController {

    private static final String VIEW = "/courses/courseDetail.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("courseId") != null) {
            int courseId = parseInt(request.getParameter("courseId"));
            EntityManagerFactory factory = null;
            try {
                factory = DBUtil.getEmFactory();
                DBCourse dbCourse = new DBCourse(factory);
                Course course = dbCourse.findCourse(courseId);
                DBDepartment dbDepartment = new DBDepartment(factory);
                List<Department> departments = dbDepartment.findDepartmentEntities();
                DBQualifications dbQual = new DBQualifications(factory);
                List<QualificationView> quals = dbQual.findQualificationsEntitiesByCourse(course);
                String deptName = null;
                String deptShortName = null;
                for (Department dept : departments) {
                    if (course.getDepartmentId().intValue() == dept.getDepartmentId().intValue()) {
                        deptName = dept.getDepartmentName();
                        deptShortName = dept.getShortName();
                    }
                }
                if (course == null) {
                    setAlertMessage(request, "Course was not found!", AlertMessageType.Error);
                } else {
                    request.setAttribute("courseId", course.getId());
                    request.setAttribute("title", course.getShortTitle());
                    request.setAttribute("credits", course.getCreditHours());
                    request.setAttribute("courseNumber", course.getCourseNumber());
                    request.setAttribute("departmentId", course.getDepartmentId());
                    request.setAttribute("description", course.getDescription());
                    request.setAttribute("departmentName", deptName);
                    request.setAttribute("deptShortName", deptShortName);
                    request.setAttribute("departments", departments);
                    request.setAttribute("qualifications", quals);
                }

            } catch (Exception ex) {
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        } else {
            EntityManagerFactory factory = null;
            try {
                factory = DBUtil.getEmFactory();
                DBDepartment dbDepartment = new DBDepartment(factory);
                List<Department> departments = dbDepartment.findDepartmentEntities();
                request.setAttribute("departments", departments);
            } catch (Exception ex) {
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        }
        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Method that handles POST actions in the /courseDetail.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;
        try {
            factory = DBUtil.getEmFactory();
            DBCourse dbCourse = new DBCourse(factory);

            boolean isNew = false;
            Course course;

            if (request.getParameter("courseId") != null && !request.getParameter("courseId").isEmpty()) {
                int courseId = parseInt(request.getParameter("courseId"));
                course = dbCourse.findCourse(courseId);
            } else {
                course = new Course();
                isNew = true;
            }
            course.setShortTitle(request.getParameter("title"));
            course.setCreditHours(parseInt(request.getParameter("credits")));
            course.setCourseNumber(parseInt(request.getParameter("courseNumber")));
            course.setDescription(request.getParameter("description"));
            course.setDepartmentId(parseInt(request.getParameter("departmentId")));

            if (isNew) {
                dbCourse.create(course);
            } else {
                dbCourse.edit(course);
            }
            //Save qualifications
            DBQualifications dbQualification = new DBQualifications(factory);
            DBInstructors dbInstructor = new DBInstructors(factory);
            
            List<QualificationView> qualifications = dbQualification.findQualificationsEntitiesByCourse(course);

            String[] instructorIds = request.getParameterValues("qualificationInstructorIds");
            List<Qualification> qualsToCreate = new ArrayList<>();

            for (String instructorId : instructorIds) {
                if (instructorId != null && !instructorId.isEmpty()) {
                    int instructorIdInt = parseInt(instructorId);
                    boolean exists = qualifications.stream().anyMatch(q -> q.getInstructorId().getInstructorId() == instructorIdInt);
                    boolean existsInCreate = qualsToCreate.stream().anyMatch(q -> q.getInstructorId().getInstructorId() == instructorIdInt);
                    Instructor instructor = dbInstructor.findInstructors(instructorIdInt);
                    
                    if (!exists && !existsInCreate && instructor != null) {
                        Qualification newQual = new Qualification();
                        newQual.setCourseId(course);
                        newQual.setInstructorId(instructor);

                        qualsToCreate.add(newQual);
                    }
                }
            }
            
            dbQualification.create(qualsToCreate);

            
        } catch (Exception ex) {
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        setAlertMessage(request, "Course saved successfully!", AlertMessageType.Success);
        response.sendRedirect("courses");
    }
    
    /**
     * Method that handles DELETE calls in the /courseDetail
     * @param request the HTTP request
     * @param response the HTTP response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            if (request.getParameter("qualificationId") != null) {
                factory = DBUtil.getEmFactory();
                DBQualifications dbQualification = new DBQualifications(factory);
                int q = parseInt(request.getParameter("qualificationId"));

                dbQualification.destroy(q);
            }

        } catch (Exception ex) {
            //TODO: implement logging exceptions
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
