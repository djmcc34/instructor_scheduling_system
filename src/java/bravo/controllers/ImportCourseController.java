/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import static bravo.controllers.BaseController.DEFAULT_ERROR_MESSAGE;
import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.utils.AlertMessageType;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.InputStreamReader;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller handles HTTP requests for /courseImport.
 * @author Team Bravo.
 */
@MultipartConfig
@WebServlet(name = "ImportCourseController", urlPatterns = {"/courseImport"})
public class ImportCourseController extends BaseController {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("courses");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Course> courses = null;
        EntityManagerFactory factory = null;
        try {
            courses = new CsvToBeanBuilder(new InputStreamReader(request.getPart("file").getInputStream()))
                    .withType(Course.class).build().parse();
            factory = DBUtil.getEmFactory();
            DBCourse dbCourse = new DBCourse(factory);
            DBDepartment dbDepartment = new DBDepartment(factory);
            for (Course course : courses) {
                switch (course.getDepartmentShortName().toUpperCase()) {
                    case "COMP":
                        course.setDepartmentId(dbDepartment.findDepartmentByShortName("COMP".toUpperCase()).getDepartmentId());
                        break;
                    case "WEBD":
                        course.setDepartmentId(dbDepartment.findDepartmentByShortName("WEBD".toUpperCase()).getDepartmentId());
                        break;
                    case "ITEC":
                        course.setDepartmentId(dbDepartment.findDepartmentByShortName("ITEC".toUpperCase()).getDepartmentId());
                        break;
                    case "ISEC":
                        course.setDepartmentId(dbDepartment.findDepartmentByShortName("ISEC".toUpperCase()).getDepartmentId());
                        break;
                    case "MIS":
                        course.setDepartmentId(dbDepartment.findDepartmentByShortName("MIS".toUpperCase()).getDepartmentId());
                        break;
                }
                dbCourse.create(course);
            }
        } catch (Exception ex) {
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
        response.sendRedirect("courses");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
