/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.*;
import bravo.models.*;
import bravo.utils.AlertMessageType;
import bravo.utils.DateUtils;
import bravo.utils.MailUtil;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for interactions with /scheduleCourse
 * @author Team Bravo
 */
@WebServlet(name = "ScheduleCourseController", urlPatterns = {"/scheduleCourse"})
public class ScheduleCourseController extends BaseController {

    private static final String VIEW = "/schedule/scheduleCourse.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();
            DBSchedule dbSchedule = new DBSchedule(factory);
            DBDepartment dbDepartment = new DBDepartment(factory);
            DBInstructors dbInstructor = new DBInstructors(factory);

            List<Department> departments = dbDepartment.findDepartmentEntities();

            departments = departments.stream()
                    .sorted((d1, d2) -> d1.getDepartmentName().compareTo(d2.getDepartmentName()))
                    .collect(Collectors.toList());

            request.setAttribute("departments", departments);
            request.setAttribute("selectedDepartmentId", request.getParameter("departmentId"));
            request.setAttribute("terms", getTerms());
            request.setAttribute("years", getYears());
            request.setAttribute("locations", getLocations());
            request.setAttribute("statuses", getCourseStatuses());

            if (request.getParameter("scheduleId") != null) {
                int scheduleId = parseInt(request.getParameter("scheduleId"));
                Schedule schedule = dbSchedule.findSchedule(scheduleId);

                request.setAttribute("scheduleId", scheduleId);
                request.setAttribute("schedule", schedule);

                Course course = schedule.getCourse();
                Department department = dbDepartment.findDepartment(course.getDepartmentId());
                List<Instructor> qualifiedInstructors = dbInstructor.findByQualified(course);

                request.setAttribute("qualifiedInstructors", qualifiedInstructors);
                request.setAttribute("department", department);
                request.setAttribute("course", course);
                request.setAttribute("formattedStartDate", DateUtils.formatDate(schedule.getStartDate()));
                request.setAttribute("formattedEndDate", DateUtils.formatDate(schedule.getEndDate()));
                request.setAttribute("formattedStartTime", DateUtils.format24HourTime(schedule.getStartTime()));
                request.setAttribute("formattedEndTime", DateUtils.format24HourTime(schedule.getEndTime()));

                if (schedule.getCourseDays() != null && !schedule.getCourseDays().isEmpty()) {
                    DateFormatSymbols dfs = new DateFormatSymbols();
                    List<String> allDays = Arrays.asList(dfs.getWeekdays()).stream().filter(x -> !x.isEmpty()).collect(Collectors.toList());
                    List<String> courseDays = Arrays.asList(schedule.getCourseDays().split(", "));

                    for (String day : allDays) {
                        if (courseDays.contains(day)) {
                            request.setAttribute("is" + day, true);
                        }
                    }
                }
            } else {

            }

        } catch (Exception ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles HTTP POST interactions.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();
            DBSchedule dbSchedule = new DBSchedule(factory);
            DBCourse dbCourse = new DBCourse(factory);
            DBInstructors dbInstructor = new DBInstructors(factory);

            boolean isNew = false;
            boolean sendInstructorRequestEmail = false;
            int previousInstructorId = 0;
            Schedule schedule = null;

            if (request.getParameter("scheduleId") != null && !request.getParameter("scheduleId").isEmpty()) {
                schedule = dbSchedule.findSchedule(Integer.parseInt(request.getParameter("scheduleId")));
                previousInstructorId = schedule.getInstructor().getInstructorId();
            } else {
                isNew = true;
                schedule = new Schedule();

                int courseId = Integer.parseInt(request.getParameter("courseId"));
                int instructorId = Integer.parseInt(request.getParameter("instructorId"));

                Course course = dbCourse.findCourse(courseId);
                Instructor instructor = dbInstructor.findInstructors(instructorId);

                schedule.setCourse(course);
                schedule.setInstructor(instructor);

                if (request.getParameter("status").equalsIgnoreCase("pending")) {
                    sendInstructorRequestEmail = true;
                }
            }

            schedule.setTerm(request.getParameter("term"));
            schedule.setYear(Integer.parseInt(request.getParameter("year")));
            schedule.setSection(request.getParameter("section"));
            schedule.setLocation(request.getParameter("location"));
            schedule.setStatus(request.getParameter("status"));

            schedule.setStartDate(convertStringToDate(request.getParameter("startDate")));
            schedule.setEndDate(convertStringToDate(request.getParameter("endDate")));

            if (request.getParameter("startTime") != null && !request.getParameter("startTime").isEmpty()) {
                schedule.setStartTime(convertStringTotime(request.getParameter("startTime")));
            } else {
                schedule.setStartTime(null);
            }

            if (request.getParameter("endTime") != null && !request.getParameter("endTime").isEmpty()) {
                schedule.setEndTime(convertStringTotime(request.getParameter("endTime")));
            } else {
                schedule.setEndTime(null);
            }

            schedule.setCourseDays(getCourseDays(request));

            if (isNew) {
                dbSchedule.create(schedule);
            } else {
                dbSchedule.edit(schedule);

                if (schedule.getStatus().equalsIgnoreCase("pending") && previousInstructorId != Integer.parseInt(request.getParameter("instructorId"))) {
                    sendInstructorRequestEmail = true;
                }
            }

            //send the email if this is a new schedule and it is in the pending status
            //or if the instructor has changed and it is in the pending status
            if (sendInstructorRequestEmail) {
                MailUtil.sendConfirmationEmail(schedule);
                setAlertMessage(request, "Course scheduled successfully! A confirmation request email has been sent to the instructor.", AlertMessageType.Success);
            } else {
                setAlertMessage(request, "Course scheduled successfully!", AlertMessageType.Success);
            }

            response.sendRedirect("schedule");
        } catch (Exception ex) {
            //TODO: Log the exception
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            response.sendRedirect("scheduleCourse");
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }

    private String getCourseDays(HttpServletRequest request) {
        List<String> days = new ArrayList<>();

        DateFormatSymbols dfs = new DateFormatSymbols();
        List<String> allDays = Arrays.asList(dfs.getWeekdays()).stream().filter(x -> !x.isEmpty()).collect(Collectors.toList());

        for (String day : allDays) {
            if (isCheckboxChecked(request, day)) {
                days.add(day);
            }
        }

        if (days.isEmpty()) {
            return null;
        }

        return String.join(", ", days);
    }
}
