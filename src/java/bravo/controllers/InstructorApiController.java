/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import controllerModels.InstructorApiModel;
import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBInstructors;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Department;
import bravo.models.Instructor;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller handles HTTP requests for /api/instructor.
 * @author Team Bravo.
 */
@WebServlet(name = "InstructorApiController", urlPatterns = {"/api/instructor"})
public class InstructorApiController extends BaseController {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();
            DBInstructors dbInstructor = new DBInstructors(factory);

            List<Instructor> instructors = dbInstructor.findInstructorsEntities();

            List<InstructorApiModel> models = new ArrayList<>();

            for (Instructor instructor : instructors) {

                String text = instructor.getFirstName() + " " + instructor.getLastName();

                InstructorApiModel model = new InstructorApiModel(instructor.getInstructorId(), text);
                models.add(model);
            }

            models = models.stream()
                    .sorted((d1, d2) -> d1.getFullName().compareTo(d2.getFullName()))
                    .collect(Collectors.toList());

            Gson gson = new Gson();
            String instructorJson = gson.toJson(models);

            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(instructorJson);
            out.flush();
        } catch (IOException ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
