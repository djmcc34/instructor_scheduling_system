/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBInstructors;
import bravo.data.DBUtil;
import bravo.models.Instructor;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller handles interaction with /instructors.
 * @author Team Bravo.
 */
@WebServlet(name = "InstructorsController", urlPatterns = {"/instructors"})
public class InstructorsController extends BaseController {

    private static final String VIEW = "/instructors/instructors.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        EntityManagerFactory factory = null;
        
        try {
            factory = DBUtil.getEmFactory();
            DBInstructors dbInstructors = new DBInstructors(factory);
            List<Instructor> instructors = dbInstructors.findInstructorsEntities();
            request.setAttribute("instructors", instructors);
            
        }
        catch (Exception ex) {
            //TOD: Log & Handle Exceptions
            System.out.println(ex);
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        }
        finally {
            if (factory != null && factory.isOpen()) {
            factory.close();
            }
        }

        request.getRequestDispatcher(VIEW).forward(request, response);
    }
    
    /**
     * Handles HTTP DELETE requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        EntityManagerFactory factory = null;
        
        try {
            if (request.getParameter("instructorId") != null) {
                factory = DBUtil.getEmFactory();
                
                DBInstructors dbInstructor = new DBInstructors(factory);
                int instructorId = parseInt(request.getParameter("instructorId"));
                
                // soft delete instructor by setting status
                Instructor instructor = dbInstructor.findInstructors(instructorId);
                instructor.setInstructorStatus("deleted");
                dbInstructor.edit(instructor);
            }
            
        }
        catch (Exception ex) {
            //TODO: implement logging exceptions
            response.sendError(500);
        }
        finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
