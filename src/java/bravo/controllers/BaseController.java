/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.utils.AlertMessageType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class that provides basic functionality used in most Controllers.
 * @author Team Bravo.
 */
public abstract class BaseController extends HttpServlet {

    /**
     * Default Error Message for the Application.
     */
    protected static final String DEFAULT_ERROR_MESSAGE = "An unexpected error has occurred, please try again or contact support if the problem persists.";

    /**
     * Default SQL Error Message for the Application.
     */
    protected static final String SQL_ERROR_MESSAGE = "An error occures while connecting to the database, please try again or contact support if the problem persists.";

    /**
     * Method to set an alert message for the controller.
     * @param request The Servlet Request.
     * @param alertMessage The Alert Message to set.
     * @param alertMessageType The Alert Message type to set.
     */
    protected void setAlertMessage(HttpServletRequest request, String alertMessage, AlertMessageType alertMessageType) {
        HttpSession session = request.getSession();
        session.setAttribute("alertMessage", alertMessage);

        if (null != alertMessageType) {
            switch (alertMessageType) {
                case Success:
                    session.setAttribute("alertClass", "alert-success");
                    break;
                case Info:
                    session.setAttribute("alertClass", "alert-info");
                    break;
                case Warning:
                    session.setAttribute("alertClass", "alert-warning");
                    break;
                case Error:
                    session.setAttribute("alertClass", "alert-danger");
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Method used to populate the list of available terms.
     * This was used as Terms were not implemented in our database.
     * @return The list of Term options.
     */
    protected List<String> getTerms() {
        List<String> terms = new ArrayList<>();

        terms.add("Spring");
        terms.add("Summer");
        terms.add("Fall");

        return terms;
    }

    /**
     * Method used to populate and control what years are displayed in the avilable year drop downs.
     * Currently limited to last year and the next two years.
     * @return the list of Years that can be selected.
     */
    protected List<Integer> getYears() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        List<Integer> years = new ArrayList<>();

        for (int i = (currentYear - 1); i < (currentYear + 2); i++) {
            years.add(i);
        }

        return years;
    }

    /**
     * Method to get a list of terms from a specified year.
     * @return The list of terms.
     */
    protected List<String> getTermWithYears() {
        List<String> terms = getTerms();
        List<Integer> years = getYears();

        List<String> termsWithYears = new ArrayList<>();

        for (Integer year : years) {
            for (String term : terms) {
                termsWithYears.add(term + " " + year);
            }
        }

        return termsWithYears;
    }

    /**
     * Method that populates the locations drop down boxes.
     * @return The list of possible Course locations.
     */
    protected List<String> getLocations() {
        List<String> locations = new ArrayList<>();

        locations.add("Online");
        locations.add("Downtown");
        locations.add("Holden, OH - LCC");
        locations.add("Dublin");
        locations.add("Nelson, OH - HOC");
        locations.add("Brunswick, OH - CCC");
        locations.add("Shelby, OH - NCSC");

        return locations;
    }

    /**
     * Method that populates the list of possible Course Instructor Status.
     * @return The list of options available for Course Status.
     */
    protected List<String> getCourseStatuses() {
        List<String> statuses = new ArrayList<>();

        statuses.add("Pending");
        statuses.add("Approved");

        return statuses;
    }

    /**
     * Utility used to Simple Format Dates.
     * @param dateString The date to format.
     * @return the formatted date.
     * @throws ParseException
     */
    protected Date convertStringToDate(String dateString) throws ParseException {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date date = format.parse(dateString);

        return date;
    }

    /**
     * Utility used to Simple format Time.
     * @param timeString the time to format.
     * @return the formatted Time.
     * @throws ParseException
     */
    protected Date convertStringTotime(String timeString) throws ParseException {
        DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        Date date = format.parse(timeString);

        return date;
    }

    /**
     * Method used to determine if check boxes are checked.
     * @param request The request to check.
     * @param name The parameter to of the request to check.
     * @return true if checked.
     */
    protected boolean isCheckboxChecked(HttpServletRequest request, String name) {
        boolean isChecked = false;

        if (request.getParameter(name) != null) {
            isChecked = request.getParameter(name).contains("on");
        }

        return isChecked;
    }
}
