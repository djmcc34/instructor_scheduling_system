/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Department;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Controller handles HTTP requests for /api/courses.
 * @author Team Bravo.
 */
@WebServlet(name = "CoursesApiController", urlPatterns = {"/api/courses"})
public class CoursesApiController extends BaseController {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();
            DBCourse dbCourses = new DBCourse(factory);
            DBDepartment dbDepartment = new DBDepartment(factory);

            List<Course> courses = null;

            if (request.getParameter("departmentId") != null) {
                //get courses by department
                Department department = dbDepartment.findDepartment(Integer.parseInt(request.getParameter("departmentId")));
                courses = dbCourses.findByDepartment(department);
            } else {
                courses = dbCourses.findCourseEntities();
            }

            List<CourseApiModel> models = new ArrayList<>();

            for (Course course : courses) {
                Department department = dbDepartment.findDepartment(course.getDepartmentId());

                String text = department.getShortName() + course.getCourseNumber()
                        + " - " + course.getShortTitle();

                CourseApiModel model = new CourseApiModel(course.getId(), text);
                models.add(model);
            }

            models = models.stream()
                    .sorted((d1, d2) -> d1.getText().compareTo(d2.getText()))
                    .collect(Collectors.toList());

            Gson gson = new Gson();
            String coursesJson = gson.toJson(models);

            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(coursesJson);
            out.flush();
        } catch (Exception ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }

    class CourseApiModel implements Serializable {

        private String text;
        private int courseId;

        public CourseApiModel() {
        }

        public CourseApiModel(int courseId, String text) {
            this.courseId = courseId;
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getCourseId() {
            return courseId;
        }

        public void setCourseId(int courseId) {
            this.courseId = courseId;
        }
    }
}
