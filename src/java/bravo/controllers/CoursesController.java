/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Department;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static java.lang.Integer.parseInt;

/**
 * Controller that handles interaction within /courses
 * @author Team Bravo
 */
@WebServlet(name = "CoursesController", urlPatterns = {"/courses"})
public class CoursesController extends BaseController {

    private static final String VIEW = "/courses/courses.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        EntityManagerFactory factory = null;
        List<Course> courses = null;
        try {
            factory = DBUtil.getEmFactory();
            DBCourse dbCourses = new DBCourse(factory);
            courses = dbCourses.findCourseEntities();
            DBDepartment dbDepartment = new DBDepartment(factory);
            String deptName = null;
            String deptShortName = null;
            Department dept = null;
            for (Course course : courses) {
                dept = dbDepartment.findDepartment(course.getDepartmentId());
                deptName = dept.getDepartmentName();
                deptShortName = dept.getShortName();
                course.setDepartmentName(deptName);
                course.setDepartmentShortName(deptShortName);
            }
        }
        catch (Exception ex) {
            //TODO: Log & Handle Exceptions
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        }
        finally {
            if (factory != null && factory.isOpen()) {
            factory.close();
            }
        }
        request.setAttribute("courses", courses);
        request.getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles DELETE requests within /courses.
     * @param request the HTTP Request.
     * @param response the HTTP Response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        EntityManagerFactory factory = null;
        
        try {
            if (request.getParameter("courseId") != null) {
                factory = DBUtil.getEmFactory();

                DBCourse dbCourse = new DBCourse(factory);

                int courseId = parseInt(request.getParameter("courseId"));

                Course course = dbCourse.findCourse(courseId);
                dbCourse.delete(courseId);
            }
        } catch (Exception ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
