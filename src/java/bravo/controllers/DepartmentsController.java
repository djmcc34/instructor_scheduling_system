/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBDepartment;
import bravo.data.DBUtil;
import bravo.models.Department;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller for interactions with /departments.
 * @author Team Bravo.
 */
@WebServlet(name = "DepartmentsController", urlPatterns = {"/departments"})
public class DepartmentsController extends BaseController {

    private static final String VIEW = "/departments/departments.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();

            DBDepartment dbDepartment = new DBDepartment(factory);

            List<Department> departments = dbDepartment.findDepartmentEntities();

            request.setAttribute("departments", departments);
        } catch (Exception ex) {
            //TODO: Log the exception
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Method for handling DELETE requests from /departments.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            if (request.getParameter("departmentId") != null) {
                factory = DBUtil.getEmFactory();

                DBDepartment dbDepartment = new DBDepartment(factory);

                int departmentId = parseInt(request.getParameter("departmentId"));

                Department dept = dbDepartment.findDepartment(departmentId);
                dbDepartment.delete(departmentId);
            }
        } catch (Exception ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
