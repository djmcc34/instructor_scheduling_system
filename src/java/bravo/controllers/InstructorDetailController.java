/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBCourse;
import bravo.data.DBDepartment;
import bravo.data.DBInstructors;
import bravo.data.DBQualifications;
import bravo.models.Qualification;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Instructor;
import bravo.models.QualificationView;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller handles HTTP requests for /instructorDetail.
 * @author Team Bravo.
 */
@WebServlet(name = "InstructorDetailController", urlPatterns = {"/instructorDetail"})
public class InstructorDetailController extends BaseController {

    private static final String VIEW = "/instructors/instructorDetail.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In doGet");

        if (request.getParameter("instructorId") != null) {
            int instructorId = 0;
            EntityManagerFactory factory = null;

            try {
                System.out.println("In doGet try");
                factory = DBUtil.getEmFactory();
                instructorId = parseInt(request.getParameter("instructorId"));
                DBInstructors dbInstructor = new DBInstructors(factory);
                DBQualifications dbQualification = new DBQualifications(factory);
                Instructor instructor = dbInstructor.findInstructors(instructorId);
                List<QualificationView> qual = dbQualification.findQualificationsEntitiesByInstructor(instructor);

                if (instructor == null) {
                    setAlertMessage(request, "Instructor was not found!", AlertMessageType.Error);
                } else {
                    request.setAttribute("instructorId", instructor.getInstructorId());
                    request.setAttribute("instructorFirstName", instructor.getFirstName());
                    request.setAttribute("instructorMiddleInitial", instructor.getMiddleInitial());
                    request.setAttribute("instructorLastName", instructor.getLastName());
                    request.setAttribute("instructorStatus", instructor.getInstructorStatus());
                    request.setAttribute("instructorEmail", instructor.getEmail());
                    request.setAttribute("instructorAdjunct", instructor.getIsadjunct());
                    request.setAttribute("instructorPhone", instructor.getPhoneNumber());
                    request.setAttribute("qualifications", qual);
                }
            } catch (Exception ex) {
                System.out.println(ex);
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        }
        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles HTTP POST requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            factory = DBUtil.getEmFactory();
            DBInstructors dbInstructors = new DBInstructors(factory);
            boolean isNew = false;
            Instructor instructor;

            if (request.getParameter("instructorId") != null && !request.getParameter("instructorId").isEmpty()) {
                // instructor exists go get it
                int instructorId = parseInt(request.getParameter("instructorId"));
                instructor = dbInstructors.findInstructors(instructorId);
            } else {
                //working with new instructor
                instructor = new Instructor();
                isNew = true;
            }

            // updating instructor information
            instructor.setFirstName(request.getParameter("instructorFirstName"));
            instructor.setLastName(request.getParameter("instructorLastName"));
            instructor.setMiddleInitial(request.getParameter("instructorMiddleInitial"));
            instructor.setEmail(request.getParameter("instructorEmail"));
            instructor.setPhoneNumber(request.getParameter("instructorPhone"));
            instructor.setInstructorStatus(request.getParameter("instructorStatus"));

            //If an html checkbox is checked it will send "on" to the server,
            //otherwise it will be null
            Boolean isAdjunct = request.getParameter("isAdjuct") != null ? request.getParameter("isAdjuct").equalsIgnoreCase("on") : false;
            instructor.setIsadjunct(isAdjunct);

            // change database
            if (isNew) {
                dbInstructors.create(instructor);
            } else {
                dbInstructors.edit(instructor);
            }

            //Save qualifications
            DBQualifications dbQualification = new DBQualifications(factory);
            DBCourse dbCourse = new DBCourse(factory);
            
            List<QualificationView> qualifications = dbQualification.findQualificationsEntitiesByInstructor(instructor);

            String[] courseIds = request.getParameterValues("qualificationCourseIds");
            List<Qualification> qualsToCreate = new ArrayList<>();

            for (String courseId : courseIds) {
                if (courseId != null && !courseId.isEmpty()) {
                    int courseIdInt = parseInt(courseId);
                    boolean exists = qualifications.stream().anyMatch(q -> q.getCourseId().getId() == courseIdInt);
                    boolean existsInCreate = qualsToCreate.stream().anyMatch(q -> q.getCourseId().getId() == courseIdInt);
                    Course course = dbCourse.findCourse(courseIdInt);
                    
                    if (!exists && !existsInCreate && course != null) {
                        Qualification newQual = new Qualification();
                        newQual.setCourseId(course);
                        newQual.setInstructorId(instructor);

                        qualsToCreate.add(newQual);
                    }
                }
            }
            
            dbQualification.create(qualsToCreate);
        } catch (Exception ex) {
            System.out.println(ex);
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }

        }

        setAlertMessage(request, "Instructor saved successfully!", AlertMessageType.Success);
        response.sendRedirect("instructors");
    }

    /**
     * Handles HTTP DELETE requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            if (request.getParameter("qualificationId") != null) {
                factory = DBUtil.getEmFactory();
                DBQualifications dbQualification = new DBQualifications(factory);
                int q = parseInt(request.getParameter("qualificationId"));

                dbQualification.destroy(q);
            }

        } catch (Exception ex) {
            //TODO: implement logging exceptions
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }
}
