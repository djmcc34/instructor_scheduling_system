/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBDepartment;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bravo.data.DBSchedule;
import bravo.data.DBUtil;
import bravo.models.Course;
import bravo.models.Department;
import bravo.models.Schedule;
import bravo.utils.AlertMessageType;
import controllerModels.ScheduleModel;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.EntityManagerFactory;

/**
 * Controller that interacts with /schedule.
 * @author Team Bravo.
 */
@WebServlet(name = "ScheduleController", urlPatterns = {"/schedule"})
public class ScheduleController extends BaseController {

    private static final String VIEW = "/schedule/schedule.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            List<String> termsWithYears = getTermWithYears();

            request.setAttribute("terms", termsWithYears);
            String selectedTerm = null;

            if (request.getParameter("term") != null
                    && !request.getParameter("term").isEmpty()
                    && termsWithYears.stream().anyMatch(x -> x.equals(request.getParameter("term")))) {
                request.setAttribute("selectedTerm", request.getParameter("term"));
                selectedTerm = request.getParameter("term");
            }

            factory = DBUtil.getEmFactory();
            DBDepartment dbDepartment = new DBDepartment(DBUtil.getEmFactory());
            DBSchedule dbSchedule = new DBSchedule(DBUtil.getEmFactory());

            List<Department> departments = dbDepartment.findDepartmentEntities();

            departments = departments.stream()
                    .sorted((d1, d2) -> d1.getDepartmentName().compareTo(d2.getDepartmentName()))
                    .collect(Collectors.toList());

            request.setAttribute("departments", departments);

            Department selectedDepartment = null;

            if (request.getParameter("departmentId") != null
                    && !request.getParameter("departmentId").isEmpty()
                    && departments.stream().anyMatch(x -> x.getDepartmentId() == parseInt(request.getParameter("departmentId")))) {
                int departmentId = parseInt(request.getParameter("departmentId"));
                request.setAttribute("selectedDepartmentId", departmentId);
                selectedDepartment = departments.stream().filter(x -> x.getDepartmentId() == departmentId).findFirst().get();
            }

            List<Schedule> scheduleEntities = null;

            if (selectedDepartment == null && selectedTerm == null) {
                //get all
                scheduleEntities = dbSchedule.findScheduleEntities();
            } else if (selectedDepartment != null && selectedTerm == null) {
                //get by department
                scheduleEntities = dbSchedule.findByDepartment(selectedDepartment);
            } else if (selectedDepartment == null && selectedTerm != null) {
                String term = selectedTerm.split(" ")[0];
                int year = parseInt(selectedTerm.split(" ")[1]);

                //get by term
                scheduleEntities = dbSchedule.findByTerm(term, year);
            } else {
                String term = selectedTerm.split(" ")[0];
                int year = parseInt(selectedTerm.split(" ")[1]);

                //get by department and term
                scheduleEntities = dbSchedule.findByDepartmentAndTerm(selectedDepartment, term, year);
            }

            List<ScheduleModel> models = new ArrayList<>();

            for (Schedule schedule : scheduleEntities) {
                Course course = schedule.getCourse();
                Department courseDepartment = departments.stream().filter(x -> Objects.equals(x.getDepartmentId(), course.getDepartmentId())).findFirst().get();

                ScheduleModel model = new ScheduleModel(schedule, course, schedule.getInstructor(), courseDepartment);

                models.add(model);
            }

            request.setAttribute("scheduleEntities", models);
        } catch (Exception ex) {
            //TOD: Log & Handle Exceptions
            System.out.println(ex);
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        request.getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles HTTP DELETE requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;

        try {
            if (request.getParameter("scheduleId") != null) {
                factory = DBUtil.getEmFactory();

                DBSchedule dbSchedule = new DBSchedule(factory);

                int scheduleId = parseInt(request.getParameter("scheduleId"));
                dbSchedule.delete(scheduleId);
            }
        } catch (Exception ex) {
            //TODO: Log the exception
            response.sendError(500);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
    }

}
