/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;


import bravo.data.DBInstructors;
import bravo.data.DBSchedule;
import bravo.data.DBUtil;
import bravo.models.Instructor;
import bravo.models.Schedule;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.Objects;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Controller that handles interactions with /confirmation.
 * @author Team Bravo.
 */
@WebServlet(name = "EmailConfirmationController", urlPatterns = {"/confirmation"})
public class EmailConfirmationController extends BaseController {

    private static final String VIEW = "/confirmation/confirmation.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EntityManagerFactory factory = null;  
        try {
        factory = DBUtil.getEmFactory(); 
            if (request.getParameter("scheduleId") != null && request.getParameter("instructorId") != null) {
                DBSchedule sch = new DBSchedule(factory);
                DBInstructors inst = new DBInstructors(factory);
                Schedule schedule = sch.findSchedule(parseInt(request.getParameter("scheduleId")));
                Instructor instructor = inst.findInstructors(parseInt(request.getParameter("instructorId")));
                
                // make sure instructor hasn't changed since email was sent
                if (Objects.equals(schedule.getInstructor().getInstructorId(), instructor.getInstructorId()) &&
                        schedule.getStatus().equalsIgnoreCase("pending")){

                    

                    request.setAttribute("name", instructor.getFirstName() + " " + instructor.getLastName());
                    request.setAttribute("cnumber", schedule.getCourse().getCourseNumber());
                    request.setAttribute("cname", schedule.getCourse().getShortTitle());
                    request.setAttribute("valid", true);
                    request.setAttribute("scheduleId", schedule.getScheduleId());
                    System.out.println("The Schedule Id is: " + schedule.getScheduleId());
                }
                else {
                    setAlertMessage(request, "An Error Occured: Either this request has already been processed or instructor assignment has changed, please contact Franklin University.", AlertMessageType.Error);
                }
                
                
            }
        
        } catch (Exception ex) {
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * Handles the HTTP POST requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String choice = request.getParameter("accepted");
        EntityManagerFactory factory = null;
        if(choice != null && choice.equalsIgnoreCase("Accept")) {
              
            try {
            factory = DBUtil.getEmFactory(); 
            System.out.println("Should be getting the scheduleId below this:");
            System.out.println(request.getParameter("scheduleId"));
                if (request.getParameter("scheduleId") != null) {
                    DBSchedule sch = new DBSchedule(factory);
                    Schedule schedule = sch.findSchedule(parseInt(request.getParameter("scheduleId")));
                    

                    schedule.setStatus("Approved");
                    sch.edit(schedule);
                }

            } catch (Exception ex) {
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        }
        else {
            try {
            factory = DBUtil.getEmFactory(); 
                if (request.getParameter("scheduleId") != null) {
                    DBSchedule sch = new DBSchedule(factory);
                    Schedule schedule = sch.findSchedule(parseInt(request.getParameter("scheduleId")));
                    

                    schedule.setStatus("Declined");
                    sch.edit(schedule);
                }

            } catch (Exception ex) {
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        }



        setAlertMessage(request, "Instructor Updated!", AlertMessageType.Success);
        response.sendRedirect("confirmation/thanks.jsp");
    }
    

}
