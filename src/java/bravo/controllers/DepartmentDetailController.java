/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBDepartment;
import bravo.data.DBUtil;
import bravo.models.Department;
import bravo.utils.AlertMessageType;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller that handles interaction on /departmentDetail.
 * @author Team Bravo.
 */
@WebServlet(name = "DepartmentDetailController", urlPatterns = {"/departmentDetail"})
public class DepartmentDetailController extends BaseController {

    private static final String VIEW = "/departments/departmentDetail.jsp";

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("departmentId") != null) {
            int departmentId = parseInt(request.getParameter("departmentId"));

            EntityManagerFactory factory = null;
            try {
                factory = DBUtil.getEmFactory();
                DBDepartment dbDepartment = new DBDepartment(factory);
                Department department = dbDepartment.findDepartment(departmentId);

                if (department == null) {
                    setAlertMessage(request, "Department was not found!", AlertMessageType.Error);
                } else {
                    request.setAttribute("departmentId", department.getDepartmentId());
                    request.setAttribute("departmentName", department.getDepartmentName());
                    request.setAttribute("shortName", department.getShortName());
                }
            } catch (Exception ex) {
                setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
            } finally {
                if (factory != null && factory.isOpen()) {
                    factory.close();
                }
            }
        }

        getServletContext()
                .getRequestDispatcher(VIEW).forward(request, response);
    }

    /**
     * handles POST requests for /departmentDetail.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;
        try {
            factory = DBUtil.getEmFactory();
            DBDepartment dbDepartment = new DBDepartment(factory);

            boolean isNew = false;
            Department department;

            if (request.getParameter("departmentId") != null && !request.getParameter("departmentId").isEmpty()) {
                int departmentId = parseInt(request.getParameter("departmentId"));
                department = dbDepartment.findDepartment(departmentId);
            } else {
                department = new Department();
                isNew = true;
            }

            department.setDepartmentName(request.getParameter("departmentName"));
            department.setShortName(request.getParameter("shortName"));
            department.setIsDeleted(false);

            if (isNew) {
                dbDepartment.create(department);
            } else {
                dbDepartment.edit(department);
            }

            setAlertMessage(request, "Department saved successfully!", AlertMessageType.Success);
        } catch (Exception ex) {
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }

        response.sendRedirect("departments");
    }
}
