/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bravo.controllers;

import bravo.data.DBLogin;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.persistence.EntityManagerFactory;
import bravo.data.DBUtil;
import bravo.models.Login;
import bravo.utils.AlertMessageType;
import java.util.List;
import org.mindrot.jbcrypt.BCrypt;

/**
 * Controller handles interaction with /login
 * @author Team Bravo.
 */
@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends BaseController {

    private static final String VIEW = "/authentication/login.jsp";

    /**
     * Handles HTTP GET requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (checkAlreadyLoggedIn(request)) {
            response.sendRedirect("schedule");
        } else {
            request.getRequestDispatcher(VIEW).forward(request, response);
        }
    }

    /**
     * Handles HTTP POST requests.
     * @param request the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (checkAuthentication(request)) {
            response.sendRedirect("schedule");
        } else {
            request.getRequestDispatcher(VIEW).forward(request, response);
        }

    }

    /**
     * Method for checking the authentication of the user.
     * @param request the HTTP request.
     * @return true if user is authorized.
     * @throws ServletException
     * @throws IOException
     */
    protected boolean checkAuthentication(HttpServletRequest request)
            throws ServletException, IOException {

        EntityManagerFactory factory = null;
        boolean auth = false;
        try {
            factory = DBUtil.getEmFactory();
            DBLogin dbLogin = new DBLogin(factory);

            String emailAddress = request.getParameter("emailAddress");
            String password = request.getParameter("password");
            //String hashed = BCrypt.hashpw(password, BCrypt.gensalt()); use this to hash a password, for future implementations. 
            List<Login> logins = dbLogin.findLogin(emailAddress);
            for (Login login : logins) {
                if (BCrypt.checkpw(password, login.getPassword())) {
                    setUserSession(request);
                    return true;
                }
                request.setAttribute("loginError", "error");
                return false;
            }

        } catch (Exception ex) {
            setAlertMessage(request, DEFAULT_ERROR_MESSAGE, AlertMessageType.Error);
        } finally {
            if (factory != null && factory.isOpen()) {
                factory.close();
            }
        }
        return false;
    }

    /**
     * Method to check if the user is already logged in.
     * @param request the HTTP request.
     * @return true if user is already logged in.
     * @throws ServletException
     * @throws IOException
     */
    protected boolean checkAlreadyLoggedIn(HttpServletRequest request)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        return !(session == null || session.getAttribute("user") == null);
    }

    /**
     * Method to set the user session upon sucessful login.
     * @param request the HTTP request.
     * @throws ServletException
     * @throws IOException
     */
    protected void setUserSession(HttpServletRequest request)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.setAttribute("user", "user");
    }
}
