/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerModels;

import bravo.models.*;
import bravo.utils.DateUtils;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class to assign actions to a Schedule.
 * @author Team Bravo.
 */
public class ScheduleModel implements Serializable {

    private int scheduleId;
    private String section;
    private String daysOfWeek;
    private String status;
    private String location;
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private String term;
    private int year;

    private Department courseDepartment;
    private Course course;
    private Instructor instructor;

    /**
     * Default Constructor.
     */
    public ScheduleModel() {

    }

    /**
     * Constructor with Schedule, Course and Instructor Information.
     * @param schedule The Schedule to set.
     * @param course The Course to assign.
     * @param instructor The Instructor to assign.
     * @param courseDepartment the Department to assign.
     */
    public ScheduleModel(Schedule schedule, Course course, Instructor instructor, Department courseDepartment) {
        this.scheduleId = schedule.getScheduleId();

        this.section = schedule.getSection();
        this.daysOfWeek = schedule.getCourseDays();
        this.status = schedule.getStatus();
        this.location = schedule.getLocation();
        this.startDate = schedule.getStartDate();
        this.endDate = schedule.getEndDate();
        this.startTime = schedule.getStartTime();
        this.endTime = schedule.getEndTime();
        this.term = schedule.getTerm();
        this.year = schedule.getYear();

        this.courseDepartment = courseDepartment;
        this.course = course;
        this.instructor = instructor;
    }

    /**
     * Method to get the ID of the assigned Schedule.
     * @return the Schedule ID.
     */
    public int getScheduleId() {
        return scheduleId;
    }

    /**
     * Method to set the ID of the assigned Schedule.
     * @param scheduleId the Schedule ID to set.
     */
    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * Method to get the Section of this Schedule.
     * @return the Section.
     */
    public String getSection() {
        return section;
    }

    /**
     * Method to set the Section of this Schedule.
     * @param section the Section to set.
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * Method to get the Days of the week this Course is scheduled.
     * @return the days scheduled.
     */
    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    /**
     * Method to set the days of the week this Course is scheduled.
     * @param daysOfWeek the days scheduled.
     */
    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    /**
     * Method to get the Status of the Instructor for this Scheduled Course.
     * @return the Status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Method for setting the Status of the Instructor for this Scheduled Course.
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Method for getting the Location of this Scheduled Course.
     * @return the Location.
     */
    public String getLocation() {
        return location;
    }

    /**
     * Method for setting the Location of this Scheduled Course.
     * @param location The Location to set.
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Method to get the Start Date for this Scheduled Course.
     * @return the Start Date.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Method to set the Start Date for this Scheduled Course.
     * @param startDate the Date to set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Method to get the End Date for this Scheduled Course.
     * @return the Date.
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Method to set the End Date for this Scheduled Course.
     * @param endDate the Date to set.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Method to get the Start Time for this Scheduled Course.
     * @return the Start Time.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Method to set the Start Time for this Scheduled Course.
     * @param startTime the Time to set.
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Method to get the End Time for this Scheduled Course.
     * @return the end time.
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Method to set the End Time for this Scheduled Course.
     * @param endTime the End Time to set.
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Method for getting a formatted Course Short Name.
     * @return the formatted Short Name of the Scheduled Course.
     */
    public String getFormattedCourseShortName() {
        return courseDepartment.getShortName() + course.getCourseNumber();
    }

    /**
     * Method for getting a formatted Term.
     * @return the formatted Term.
     */
    public String getFormattedTerm() {
        return this.term + " " + this.year;
    }

    /**
     * Method for getting a formatted Instructor full name.
     * @return the formatted Instructor Name.
     */
    public String getFormattedInstructorName() {
        return instructor.getFirstName() + " " + instructor.getLastName();
    }

    /**
     * Method for getting a formatted Course Start Date.
     * @return the formatted date.
     */
    public String getFormattedStartDate() {
        return DateUtils.formatDate(startDate);
    }

    /**
     * Method for getting a formatted Course End Date.
     * @return the formatted end date.
     */
    public String getFormattedEndDate() {
        return DateUtils.formatDate(endDate);
    }

    /**
     * Method for getting a formatted Course Start Time.
     * @return the formatted start time.
     */
    public String getFormattedStartTime() {
        return DateUtils.formatTime(startTime);
    }

    /**
     * Method for getting a formatted Course End Time.
     * @return the formatted end time.
     */
    public String getFormattedEndTime() {
        return DateUtils.formatTime(endTime);
    }
}
