/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerModels;

import java.io.Serializable;

/**
 * Class for controlling the Instructor API.
 * @author Team Bravo.
 */
public class InstructorApiModel implements Serializable {

    private String fullName;
    private int instructorId;

    /**
     * Default Constructor.
     */
    public InstructorApiModel() {
    }

    /**
     * Constructor with Instructor ID Full Name.
     * @param instructorId the Instructor ID.
     * @param text The Instructor Full Name.
     */
    public InstructorApiModel(int instructorId, String text) {
        this.instructorId = instructorId;
        this.fullName = text;
    }

    /**
     * Method to get the Instructor Full Name.
     * @return the Instructor Full Name.
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Method to set the Instructor Full Name.
     * @param text the Instructor Full Name to set.
     */
    public void setFullName(String text) {
        this.fullName = text;
    }

    /**
     * Method to get this Instructor ID.
     * @return this Instructor ID.
     */
    public int getInstructorId() {
        return instructorId;
    }

    /**
     * Method to set This Instructor to a supplied Course ID.
     * @param courseId the ID of the Course to assign this Instructor.
     */
    public void setInstructorId(int courseId) {
        this.instructorId = courseId;
    }
}
